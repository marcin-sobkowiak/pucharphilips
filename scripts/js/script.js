$(document).ready(function(){
    $(".nav-tabs-1 .1").click(function(){
        $(".nav-tabs-1 .2").parent().removeClass('current');
        $(".nav-tabs-1 .1").parent().addClass('current');
        $('.x-content-box-1-js-2').hide();
        $('.x-content-box-1-js-1').fadeIn();
        return false;
    });
    $(".nav-tabs-1 .2").click(function(){
        $(".nav-tabs-1 .1").parent().removeClass('current');
        $(".nav-tabs-1 .2").parent().addClass('current');
        $('.x-content-box-1-js-1').hide();
        $('.x-content-box-1-js-2').fadeIn();
        return false;
    });    
});

$(document).ready(function(){
    $('input').iCheck({
    checkboxClass: 'icheckbox',
    radioClass: 'iradio'
    });
});

$(document).ready(function(){
    $(".main-menu ul li").hoverIntent({    
        sensitivity: 1, // number = sensitivity threshold (must be 1 or higher)    
        interval: 130,  // number = milliseconds for onMouseOver polling interval    
        timeout: 400,   // number = milliseconds delay before onMouseOut    
        over:function(){
            $(this).find('ul:first')
                .removeClass("hoverOut").toggleClass("hoverIn");
        },
        out: function(){
            $(this).find('ul:first')
                .removeClass("hoverIn").toggleClass("hoverOut");
        }
    });
});

$(function(){
    var logoRsp = $('header .logo').html();
    $('.header-rsp .logo-rsp a').html(logoRsp);
    
    var dataRsp = $('header .langs-box').html();
    $('.header-rsp .data-rsp').html("<div class='langs-box'>" + dataRsp + "</div>");    
    
    $(".hamburger").click(function(){
        $(this).toggleClass('is-active');
        $('.menu-rsp').fadeToggle(200);
    });
    var mainMenu = $('.main-menu ul').html();
    $('.menu-rsp nav').html('<ul>' + mainMenu + '</ul>');
    $('.menu-rsp nav ul > li > ul').parent().find('a:first').append('<span class="btn-slide-down">&#9660;</span>');
    $('.main-menu ul > li > ul').parent().find('a:first').append('<span class="btn-slide-down">&#9660;</span>');
    $('.btn-slide-down').parent().parent().addClass('li-submenu');
    $('.btn-slide-down').click(function(){
        $(this).parent().next().stop().slideToggle(200);
        return false;
    });

});

$(function(){ 	
    var config = {
      '.chosen-select': {}
    }
    for (var selector in config) {
       $(selector).chosen(config[selector]);
    }
});

$(function(){
    $('.carousel ul').bxSlider({
        nextSelector: '.carousel .next-box',
        prevSelector: '.carousel .prev-box',	
        slideWidth: 234,
        minSlides: 5,
        maxSlides: 5,
        moveSlides: 1,
        slideMargin: 0,
        shrinkItems: true,
        touchEnabled: false         
    });
});

$(function(){
    $('.slider-1 ul').bxSlider({
        nextSelector: '.s-nav-1 .next-box',
        prevSelector: '.s-nav-1 .prev-box',        
        mode: 'fade',
        captions: true,
        controls: true,
        auto: false,
        pause: 3000,
        touchEnabled: false     
    });
    $('.slider-2 ul').bxSlider({
        nextSelector: '.s-nav-2 .next-box',
        prevSelector: '.s-nav-2 .prev-box',        
        mode: 'fade',
        captions: true,
        controls: true,
        auto: false,
        pause: 3000,
        touchEnabled: false
    });    
});

$(function(){
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('body').addClass('body-active');
        } else {
            $('body').removeClass('body-active');
        }
    });
});
