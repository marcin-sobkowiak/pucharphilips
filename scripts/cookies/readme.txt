Jak stosujemy widget?

1. W sekcji <head> dodajemy link do styl�w CSS:
<link rel="stylesheet" href="divante.cookies.min.css" type="text/css" media="all" />

2. Tu� przed zamykaj�cym znacznikiem </body> dodajemy skrypty JS:
    <script type="text/javascript" src="divante.cookies.min.js"></script>
    <script>window.jQuery.cookie || document.write('<script src="jquery.cookie.min.js"><\/script>')</script>
    <script type="text/javascript">
        jQuery.divanteCookies.render({
            privacyPolicy : true,
            cookiesPageURL : 'http://.../'
        });
    </script>

Opcje:
* privacyPolicy : true | false    // 'true' - podaj URL do polityki | 'false' - podaj URL do regulaminu
* cookiesPageURL: 'http://.../'    // podaj URL do strony z wyjasnieniem cookies

Takie dwie opcje zosta�y dodane, bo niekt�re serwisy maj� oddzieln�
polityk� prywatno�ci, a w innych polityka cookies b�dzie zmieszczona w
regulaminie serwisu.