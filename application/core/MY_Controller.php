<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
public $_actDate='';
public $_siteAdress='';
public $_siteEmail='';
public $_boolArr='';
public $_statusArr='';
public $_linkArr='';
public $_ph='';


function __construct()
{
	parent::__construct();
	/* aktualna data */
	$this->load->helper('date');
	//$this->_datestring = "%Y-%m-%d %H:%i:%s";
	//$this->_time = time();	
	$this->_actDate = mdate("%Y-%m-%d %H:%i:%s",time()); 
	$this->_edition=1; /* aktualna edycja */
	/*zmienne globalne */
	$this->_ph = array(0=>'',1=>'Aleksandrowicz Krzysztof',2=>'Frychel Jarosław',3=>'Leśniak Marcin',4=>'Pawłowski Mirosław',5=>'Pietrusiak Andrzej',6=>'Sadlej Paweł',7=>'Zabłocki Jacek');
	$this->_siteAdress = 'https://pucharphilips.pl/';	
	$this->_siteEmail ='kontakt@pucharphilips.pl';
	$this->_boolArr =array(0=>'nie',1=>'tak');
	$this->_monthArr =array('styczen'=>'Styczeń','luty'=>'Luty','marzec'=>'Marzec','kwiecien'=>'Kwiecień','maj'=>'Maj','czerwiec'=>'Czerwiec','lipiec'=>'Lipiec','sierpien'=>'Sierpień','wrzesien'=>'Wrzesień','pazdziernik'=>'Październik','listopad'=>'Listopad','grudzien'=>'Grudzień');
	$this->_statusArr =array('trwa'=>'TRWA','zakonczony'=>'ZAKOŃCZONY');
	$this->_user =$this->auth4->authUser('user_sid'); /* autoryzacja uzytkownika */
	/* ustawienie jezyka */
	/* załadowanie jezyka */
	$this->config->set_item('language', 'polish');
	
	
	$this->_admin =$this->auth4->authAdmin('admin_sid'); /* autoryzacja admina */
	// if($_SERVER['REMOTE_ADDR']=='77.65.110.184') $this->output->enable_profiler(true);

}
/* szablon głowny */	
function _display($lang,$d=array()) {	
	/* wybór templatki */
	$file = array(1=>'main/template');
	/***** powtarzalne dane  *********/
	$basicData = array();	
	/* załadowanie widoku */
	$this->load->view($file[$lang], array_merge($basicData, $d));	
	//$this->output->enable_profiler(true);
}	

/* szablon admina*/
function _displayAdmin($file, $d=array()) {
	$basicData = array();	
	/* zaladowanie widoku */
	$this->load->view($file, array_merge($basicData, $d));
	//$this->output->enable_profiler(true);
}	
	



/* dodatkowe walidatory do formularzy */

}/* End of file */?>
