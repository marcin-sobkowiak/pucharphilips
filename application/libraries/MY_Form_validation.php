<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation  extends CI_Form_validation
{
     function __construct($config = array())
     {
          parent::__construct($config);
	  $this->_CI  =& get_instance();
     }

    /** ddoatkowe walidatory 4A
     *
     * checkEmail -unikalnosc adresu email
     * checkNIP -poprawnosc NIP
     * checkPhone -unikalnosc telefonu
     * checkEmail -unikalnosc adresu email
     * checkPasswordStrength - sila hasla
     *
     *
     */
	function checkEmail($str,$id=0)
	{	
		if($id) {
			$user = $this->_CI->user_model->getSingle('id',array('email'=>$str,'id !='=>$id));
		} else {
			$user = $this->_CI->user_model->getSingle('id',array('email'=>$str));
		}
		if($str!='') {
			if(empty($user))  {
				return true;
			} else { 
				return false;
			}
		} else {
			return true;
		}
	}
	
	function checkNIP($str)
	{
		$str = preg_replace("/[^0-9]+/","",$str);
		if (strlen($str) != 10) return false;
		$arrSteps = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
		$intSum=0;
		for ($i = 0; $i < 9; $i++)
		{
			$intSum += $arrSteps[$i] * $str[$i];
		}
		$int = $intSum % 11;
	 
		$intControlNr=($int == 10)?0:$int;
		if ($intControlNr == $str[9])  return true;
		return false;
	}
	
	function  checkPhone($str,$id=0)
	{
		if($id) {
			$user = $this->user_model->getSingle('id',array('phone'=>$login,'id !='=>$id));
		} else {
			$user = $this->user_model->getSingle('id',array('phone'=>$login));
		}
		if(empty($user))  {
			return true;
		} else { 
			return false;
		}
	}
	function checkLogin($str,$id=0)
	{	
		return true;
		if($id) {
			$user = $this->user_model->getSingle('id',array('login'=>$str,'id !='=>$id));
		} else {
			$user = $this->user_model->getSingle('id',array('login'=>$str));
		}
		if($str!='') {
			if(empty($user))  {
				return true;
			} else { 
				return false;
			}
		} else {
			return true;
		}
	}
	function checkPasswordStrength($password)
	{
		$returnVal = True;
		if ( strlen($password) <8) {
			$returnVal = False;
		}
		if ( strlen($password) >60) {
			$returnVal = False;
		}
		if ( !preg_match("#[0-9]+#", $password) ) {
			$returnVal = False;
		}
		if ( !preg_match("#[a-z]+#", $password) ) {
			$returnVal = False;
		}
		if ( !preg_match("#[A-Z]+#", $password) ) {
			$returnVal = False;
		}
		return $returnVal;
	}
} 