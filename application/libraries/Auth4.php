<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter authorization Library 4 Aces 
 logUser - logowanie
 authUser - status zalogowania
 
 
 */

class Auth4 {
 //protected $CI;
 public function __construct()
        {
		$this->_CI  =& get_instance();
	        $this->_CI ->load->helper('form');
	        $this->_CI ->load->model('user_model');
	        $this->_CI ->load->model('admin_model');
	        $this->_CI ->load->library('session');
		$this->_CI->load->helper('date');	
		$this->_actDate = mdate("%Y-%m-%d %H:%i:%s", time()); 
		$this->_salt = '3458%#$53'; 
		$this->_edition =1;
        }
public function logUser($login,$pass,$cookieName,$remember=0) {
		//$password =	sha1($this->_salt.sha1($pass));
		$password =	$pass;
		$logData =$this->_CI->user_model->getSingle('id,loginCount,active',array('email'=>$login,'password'=>$password,'active'=>1)); /* sprawdza czy poprawnosc loginu i hasla */
		if (!empty($logData) && $logData['active']) {  /* poprawne dane */
			$logCount = $logData['loginCount']+1; 
			$time = time();
			$insert = array(
					'session_id' =>  $this->_CI ->session->session_id,
					'session_ip'=>$_SERVER['REMOTE_ADDR'],
					'session_browser'=> $this->_CI ->input->user_agent(),
					'session_start'=>$time,
					'session_time'=>$time,
					'lastLogin'=>$this->_actDate,
					'loginCount' => $logCount,  
				);
			$this->_CI ->db->update('users',$insert,array('id'=>$logData['id']));
			if($remember) {
				setcookie($cookieName, $this->_CI ->session->session_id,$time + (86400 * 30),'/'); 
			} else {
				setcookie($cookieName, $this->_CI ->session->session_id,0,'/'); 
			}
			$this->_CI->db->insert('usersLogin',array('idUser'=>$logData['id'],'date'=>$this->_actDate,'session_start'=>$time,'session_time'=>$time,'session_ip'=>$_SERVER['REMOTE_ADDR']));
			return array('logged'=>true,'idUser'=>$logData['id']);
		} else  {/*bledne dane */
			return array('logged'=>false);
		}
}
function authUser($cookieName) {
	$user=array('logged'=>false);
	$session =  $this->_CI ->input->cookie($cookieName);
	if($session!=False) {
		$userSession=$this->_CI->user_model->getSingle('*',array('session_id'=>$session,'session_ip'=>$_SERVER['REMOTE_ADDR'],'session_browser'=>$this->_CI ->input->user_agent(),'active'=>1));
		if(!empty($userSession)) {
			$check = false;
			$time = time();
			$diff = $time -$userSession['session_time'];
			if($diff<3600) $check = true;
			if($check) {
				$user['logged']=true;
				$user['session']=$session;
				$user['login']=$userSession['login'];
				$user['idUser']=$userSession['id'];
				$user['email']=$userSession['email'];
				$user['idRank']=$userSession['idRank'];
				$this->_CI->db->update('users',array('session_time'=>$time),array('id'=>$userSession['id']));
			}	
		}	
	}
	return $user;
}
function authAdmin($cookieName) {
	$admin=array('logged'=>false);
	$session = $this->_CI->input->cookie($cookieName);
	if($session!=False) {
		$userSession= $this->_CI->admin_model->getSingle('*',array('session_id'=>  $session,'session_ip'=>$_SERVER['REMOTE_ADDR'],'session_browser'=>$this->_CI->input->user_agent(),'active'=>1));
		if(!empty($userSession)) {
			$time = time();
			$diff = $time -$userSession['session_time'];
			if($diff<3600) {
				$admin['logged']=true;
				$admin['session']=$session;
				$admin['adminType']=$userSession['type'];
				$admin['adminName']=$userSession['name'];
				$admin['idAdmin']=$userSession['idAdmin'];
				$admin['login']=$userSession['login'];
				$admin['passwordDate']=$userSession['passwordDate'];
				$this->_CI->db->update('admins',array('session_time'=>$time),array('idAdmin'=>$userSession['idAdmin']));		
			}	
		}	
	}
	return $admin;
}

function passGenerate() {
	$smallLetter=array('a','b','c','d','e','f','g','h','j','k','m','n','p','q','r','s','y','v','w');
	$bigLetter=array('A','B','C','D','E','F','G','H','K','L','M','N','V','P','Q','R','W');
	$bigLetterRand = random_string('numeric', 1);
	$bigLetterRand2 = random_string('numeric', 1);
	$bigLetterRand3 = random_string('numeric', 1);
	$bigLetterRand4 = random_string('numeric', 1);
	$numerRand = random_string('nozero', 1);
	$numerRand2 = random_string('nozero', 1);
	$numerRand3 = random_string('nozero', 1);
	$numerRand4 = random_string('nozero', 1);
	$passwordText = $numerRand2.$bigLetter[$bigLetterRand].$bigLetter[$bigLetterRand2].$numerRand.$numerRand3.$bigLetter[$bigLetterRand3].$numerRand4.$bigLetter[$bigLetterRand4];
	$password =	sha1($this->_salt.sha1($passwordText));
	$result =array('passwordText'=>$passwordText,'password'=>$password);
	return $result;
}
}