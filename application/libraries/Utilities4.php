<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter utilities Library 4 Aces 

 
 */

class Utilities4 {
 //protected $CI;
 public function __construct()
        {
		$this->_CI  =& get_instance();
        }

function _sendEmail($text,$receiver,$subject,$from,$fromTitle='Puchar Philips') {
	$this->_CI->load->library('email');
	$config['protocol'] = 'smtp';
	$config['smtp_host'] ='';
	$config['smtp_user'] = '';
	$config['smtp_pass'] = '';
	$config['smtp_port'] = '587';
	$config['smtp_crypto'] = 'tls';
	$config['validate'] = 'TRUE';
	$config['mailpath'] = '/usr/sbin/sendmail';
	$config['charset'] = 'utf-8';
	$config['newline'] = '\r\n';
	$config['wordwrap'] = TRUE;
	$config['mailtype'] = 'html';
	$this->_CI->email->initialize($config);	
	$this->_CI->email->clear();
	$this->_CI->email->from($from,$fromTitle);
	$this->_CI->email->message($text);	
	$this->_CI->email->to($receiver);	
	$this->_CI->email->bcc('test@4aces.pl');	
	$this->_CI->email->subject($subject);                
	$this->_CI->email->send();  	
	// $this->email->print_debugger();
}

/* api curl */
function _my_curl_request($post_data)
{
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "-----");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Authorization: ----- '
	));
	
        $result = @curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
}
function _dodaj_foto($field,$katalog,$encrypt)		
{
	$config['upload_path'] = './files/'.$katalog;
	$config['allowed_types'] = 'gif|jpg|png|JPG';
	$config['max_size']	= '20480';
	$config['max_width']  = '6000';
	$config['max_height']  = '6000';
	$config['encrypt_name']  = $encrypt;
	$this->load->library('upload', $config);
	$this->upload->initialize($config);	
	if ($this->_CI->upload->do_upload($field)) // wykonuje upload pliku
	{
	return $this->_CI->upload->data();
	} else {
	return false;
	//return $this->image_lib->display_errors();
	}	
}
function _resize_foto($nazwa,$new,$width,$height,$dimension,$katalog)
{
	$config['image_library'] = 'gd2';
	$config['source_image'] = './files/'.$katalog.$nazwa;
	if ($new ==1) {$config['new_image'] = './files/'.$katalog.'min_'.$nazwa;} 
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = $width;
	$config['height'] = $height;
	$config['master_dim'] = $dimension;
	$this->_CI->load->library('image_lib');
	$this->_CI->image_lib->initialize($config); 
	$this->_CI->image_lib->resize();
	
	$this->_CI->image_lib->clear();
	return $this->_CI->image_lib->display_errors();
} 
function _crop_foto($nazwa,$width,$height,$katalog)
{
	$config['image_library'] = 'gd2';
	$config['source_image'] = './files/'.$katalog.$nazwa;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = $width;
	$config['height'] = $height;
	$config['x_axis'] = '0';
	$config['y_axis'] = '0';	
	$this->_CI->load->library('image_lib');
	$this->_CI->image_lib->initialize($config); 
	$this->_CI->image_lib->crop();
	$this->_CI->image_lib->clear();	
} 
function _dodaj_plik($field,$katalog,$encrypt)
{
	$config['upload_path'] = './files/'.$katalog.'/';
	$config['allowed_types'] = '*';
	$config['max_size']	= '20480';
	$config['encrypt_name']  = $encrypt;
	$this->_CI->load->library('upload', $config);
	$this->_CI->upload->initialize($config);	
	if ($this->_CI->upload->do_upload($field)) // wykonuje upload pliku
	{
		return $this->_CI->upload->data();
	} else {
		//return  $this->upload->display_errors();
		return false;
	}	
}  

 
function _userFriendlyPass($str) {
	$str = str_replace("l", "x", $str);
	$str = str_replace("I", "X", $str);
	$str = str_replace("o", "z", $str);
	$str = str_replace("O", "Z", $str);
	$str = str_replace("0", "F", $str);
	return $str;
}
/*wysylanie sms*/
function _sendSMS($text,$phone) {
/*$text = $this->_clear_spec_char($text);

$params = array(
'username' => '', //login z konta SMSAPI
'password' => '', //lub $password="ciąg md5"
'to' => $phone, //numer odbiorcy
'from'=>'',//nazwa nadawcy musi być aktywna
'message' =>$text, //treść wiadomości
'encoding'=>'utf-8'
);
$data = '?'.http_build_query($params);
$plik = fopen('http://api.smsapi.pl/sms.do'.$data,'r');

$wynik = fread($plik,1024); 
fclose($plik);
*/
} 
function _clear_spec_char($text){ 
   $tabela = Array( 
   //WIN 
    "\xb9" => "a", "\xa5" => "A", "\xe6" => "c", "\xc6" => "C", 
    "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L", 
    "\xf3" => "o", "\xd3" => "O", "\x9c" => "s", "\x8c" => "S", 
    "\x9f" => "z", "\xaf" => "Z", "\xbf" => "z", "\xac" => "Z", 
    "\xf1" => "n", "\xd1" => "N", 
   //UTF 
    "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C", 
    "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L", 
    "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S", 
    "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z", 
    "\xc5\x84" => "n", "\xc5\x83" => "N", 
   //ISO 
    "\xb1" => "a", "\xa1" => "A", "\xe6" => "c", "\xc6" => "C", 
    "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L", 
    "\xf3" => "o", "\xd3" => "O", "\xb6" => "s", "\xa6" => "S", 
    "\xbc" => "z", "\xac" => "Z", "\xbf" => "z", "\xaf" => "Z", 
    "\xf1" => "n", "\xd1" => "N", 
    //I to co nie potrzebne 
   "$" => "-", "!" => "-", "@" => "-", "#" => "-", "%" => "-"); 

   return strtr($text,$tabela); 
}
} /*koniec */