<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter authorization Library 4 Aces 
 logUser - logowanie
 authUser - status zalogowania
 
 
 */

class User4 {
 //protected $CI;
 public function __construct()
        {
		$this->_CI  =& get_instance();
        }

function userRounds($idUser,$lang,$edition,$actRound,$actDate) {
	$result=array();
	$rounds = $this->_CI->challenge_model->getList('dateFrom,dateTo,idRound',array('lang'=>$lang,'edition'=>$edition));
	$roundsArr=array();
	foreach($rounds as $item) {
		if($item['dateFrom']<=$actDate&&$item['dateTo'] >=$actDate) {
			$item['status'] = 'active';
		} elseif($item['dateFrom']>$actDate) {
			$item['status'] = 'inactive';
		} elseif($item['dateTo']<$actDate) {
			$results = count($this->_CI->challenge_model->getResultsRound('knowledge.id',array('lang'=>$lang,'knowledge.edition'=>$edition,'idUser'=>$idUser,'knowledge.idRound'=>$item['idRound'],'solved'=>1)));
			if($results>0) {
				$item['status'] = 'done';
			} else {
				$item['status'] = 'undone';
			}
		}
		array_push($result,$item);
	}
	return $result;
}
function userKnowledge($idUser,$lang,$edition,$actRound,$actDate) {
	$idUser = (int)$idUser;
	$result=array();
	$cond = array('lang'=>$lang,'edition'=>$edition);
	if($actRound) $cond['idRound'] =$actRound;
	$knowledge=$this->_CI->challenge_model->getListKnowledge('title,shortContent,longContent,dateFrom,dateTo,type,id,idRound',$cond);
	foreach($knowledge as $item) {	
		if($item['dateFrom']<=$actDate) {
			$read = $this->_CI->challenge_model->getSingleKnowledgeRead('date',array('idKnowledge'=>$item['id'],'idUser'=>$idUser));
			if(!empty($read)) {
				$item['status'] ='read';
			} else {
				$item['status'] ='unread';
			}
			$solved = $this->_CI->challenge_model->getSingleResult('id',array('idKnowledge'=>$item['id'],'idUser'=>$idUser));
			if(!empty($solved)) {
				$item['solved'] ='solved';
				$item['active'] =false;
				if($item['dateFrom']<=$actDate&&$item['dateTo'] >=$actDate) {	
					$item['active'] =true;
				}
			} else {
				if($item['dateFrom']<=$actDate&&$item['dateTo'] >=$actDate) {	
					$item['solved'] ='unsolved';
					$item['active'] =true;
				} else {
					$item['solved'] ='inactive';
					$item['active'] =false;
				}
			}
	//	}elseif($item['dateFrom']<=$actDate&&$item['dateTo'] >=$actDate) {	
		
		}else {
			$item['status'] ='inactive';
			$item['solved'] ='inactive';
			$item['active'] =false;
		}
		array_push($result,$item);
	}
	return $result;
}
function userSingleKnowledge($id,$idUser,$lang,$edition,$actRound,$actDate) {
	$idUser = (int)$idUser;
	/** edition, actRound juz nie sa potrzebne - wywalic w wolnej chwili */
	$result=array('access'=>false);
	$knowledge=$this->_CI->challenge_model->getSingleKnowledge('title,shortContent,longContent,dateFrom,dateTo,type,id,image',array('lang'=>$lang,'id'=>$id));
	if(!empty($knowledge)) {	
		if($knowledge['dateFrom']<=$actDate) {
			$read = $this->_CI->challenge_model->getSingleKnowledgeRead('date',array('idKnowledge'=>$id,'idUser'=>$idUser));
			if(empty($read)) $this->_CI->db->insert('knowledgeRead',array('idUser'=>$idUser,'edition'=>$edition,'idRound'=>$actRound,'idKnowledge'=>$id));
			$knowledge['gallery'] =  $this->_CI->challenge_model->getListGallery('image',array('idKnowledge'=>$id));
			$result = 	$knowledge;
			$result['access'] = true;
		}
	}
	return $result;
}
function userSingleQuiz($id,$idUser,$lang,$edition,$actRound,$actDate) {
	$idUser = (int)$idUser;
	$result=array('access'=>false,'solved'=>false,'poll'=>array());
	$knowledge=$this->_CI->challenge_model->getSingleKnowledge('dateFrom,dateTo,type,id,title',array('lang'=>$lang,'edition'=>$edition,'idRound'=>$actRound,'id'=>$id));
	if(!empty($knowledge)) {	
		if($knowledge['dateFrom']<=$actDate) {
			$poll=$this->_CI->challenge_model->getQuestions('id,text,type,image',array('active'=>1,'idKnowledge'=>$id));
			$result=array();
			$userResult = $this->_CI->challenge_model->getSingleResult('*',array('idUser'=>$idUser,'idKnowledge'=>$id));
			
			if(!empty($userResult)) {
				$pointsSum=$userResult['points'];
				$userResult = json_decode($userResult['result'],true);
				$solved=true;
			} else {
				$userResult=array();
				$solved=false;
				$pointsSum=0;
			}
			$questions =array();
			foreach($userResult as $item) {
				$answers=array();
				
				foreach($item['answers'] as $item2) {
					$answers[$item2['id']] = array('value'=>$item2['value'],'correct'=>$item2['correct'],'points'=>$item2['points']);
				}
				$questions[$item['id']] = array('answers'=>$answers);
			}
			foreach($poll as $item) {
				$correct=false;
				$question = array('id'=>$item['id'],'title'=>$item['text'],'type'=>$item['type'],'image'=>$item['image'],'points'=>0,'answers'=>array(),'correct'=>false);
				$answers = $this->_CI->challenge_model->getAnswers('id,text,correct',array('idQuestion'=>$item['id'],'active'=>1));
				foreach($answers as $item2) {
					$item2['value'] = false;
					if(!empty($questions[$item['id']]['answers'][$item2['id']]['value'])) {
						$item2['value'] = $questions[$item['id']]['answers'][$item2['id']]['value'];
					}
					if(!empty($questions[$item['id']]['answers'][$item2['id']]['points'])) {
						$question['points'] = $questions[$item['id']]['answers'][$item2['id']]['points'];
					}
					if($item2['correct']&&$item2['value']==true) {
						$correct=true;
					}
					array_push($question['answers'],$item2);
				}
				$question['correct'] = $correct;
				array_push($result,$question);
			}
			if($knowledge['dateTo'] >=$actDate) {
				
			} else {
				$solved=true;
			}
			$result['poll'] = $result;
			$result['points'] = $pointsSum;
			$result['solved'] = $solved;
			$result['knowledge'] = $knowledge;
			$result['access'] = true;
		}
	}
	return $result;
}
function userSingleResult($id,$idUser) {
	$idUser = (int)$idUser;
	$result=array();
		$userResult = $this->_CI->challenge_model->getSingleResult('*',array('idUser'=>$idUser,'idKnowledge'=>$id));
		$poll=$this->_CI->challenge_model->getQuestions('id,text,type',array('active'=>1,'idKnowledge'=>$id));
		if(!empty($userResult)) {
			$userResult = json_decode($userResult['result'],true);
			$solved=true;
		} else {
			$userResult=array();
			$solved=false;
		}
		$questions =array();
		foreach($userResult as $item) {
			$pointsQ=0;
			$correct=false;
			foreach($item['answers'] as $item2) {
				if($item2['correct']&&$item2['value']==true) {
					$correct = true;	
				}	
				if($item2['value']==true) {
					$pointsQ=$item2['points'];
				}
			}
			$questions[$item['id']] = array('points'=>$pointsQ,'correct'=>$correct);
		}
		foreach($poll as $item) {
			if($solved) {
				if(!empty($questions[$item['id']])) {
					$item['points']  = $questions[$item['id']]['points'];
					$item['correct'] =$questions[$item['id']]['correct'];
				} else {
					$item['points'] = 0;
					$item['correct'] = false;
				}
				array_push($result,$item);
			} else {
				$item['points'] = 0;
				$item['correct'] = false;
				array_push($result,$item);
			}
		}
	return $result;
}

function userSingleLifesaver($id,$idUser,$lang,$edition,$actRound,$actDate) {
	$idUser = (int)$idUser;
	$result=array('access'=>false);
	$poll=$this->_CI->challenge_model->getLifesaverQuestions('id,text,type,image',array('active'=>1,'idRound'=>$id,'lang'=>$lang));
	$result=array();
	$userResult = $this->_CI->challenge_model->getSingleLifesaverResult('*',array('idUser'=>$idUser,'idRound'=>$id));
	if(!empty($userResult)) {
		$userResult = json_decode($userResult['result'],true);
		$solved=true;
	} else {
		$userResult=array();
		$solved=false;
	}
	$questions =array();
	foreach($userResult as $item) {
		$answers=array();
		foreach($item['answers'] as $item2) {
			$answers[$item2['id']] = array('value'=>$item2['value'],'correct'=>$item2['correct'],'points'=>$item2['points']);
		}
		$questions[$item['id']] = array('answers'=>$answers);
	}
	$resultPoll=array();
	foreach($poll as $item) {
		$question = array('id'=>$item['id'],'title'=>$item['text'],'type'=>$item['type'],'image'=>$item['image'],'points'=>0,'answers'=>array(),'points'=>0);
		$answers = $this->_CI->challenge_model->getLifesaverAnswers('id,text,correct',array('idQuestion'=>$item['id'],'active'=>1));
		foreach($answers as $item2) {
			$item2['value'] = false;
			if(!empty($questions[$item['id']]['answers'][$item2['id']]['value'])) {
				$item2['value'] = $questions[$item['id']]['answers'][$item2['id']]['value'];
			}
			if(!empty($questions[$item['id']]['answers'][$item2['id']]['points'])) {
				$question['points'] = $questions[$item['id']]['answers'][$item2['id']]['points'];
			}
			array_push($question['answers'],$item2);
		}
		array_push($resultPoll,$question);
	}
			
	$result['poll'] = $resultPoll;
	$result['solved'] = $solved;
	$result['access'] = true;
	return $result;
}
function userSingleLifesaverResult($id,$idUser,$lang) {
	$idUser = (int)$idUser;
	$result=array();
		$userResult = $this->_CI->challenge_model->getSingleLifesaverResult('*',array('idUser'=>$idUser,'idRound'=>$id));
		$poll=$this->_CI->challenge_model->getLifesaverQuestions('id,text,type',array('active'=>1,'idRound'=>$id,'lang'=>$lang));
		if(!empty($userResult)) {
			$userResult = json_decode($userResult['result'],true);
			$solved=true;
		} else {
			$userResult=array();
			$solved=false;
		}
		$questions =array();
		foreach($userResult as $item) {
			$questions[$item['id']] = array('points'=>$item['pointsAnswer']);
		}
		foreach($poll as $item) {
			if($solved) {
				if(!empty($questions[$item['id']])) {
					$item['points']  = $questions[$item['id']]['points'];
				} else {
					$item['points'] = 0;
				}
				array_push($result,$item);
			} else {
				$item['points'] = 0;
				array_push($result,$item);
			}
		
		}
	return $result;
}
function userTurbo($idUser,$edition) {
	$turbo=$this->_CI->challenge_model->getTurboAnswer('idTurbo,answer',array('idUser'=>$idUser,'edition'=>$edition));
	krsort($turbo);
	if(empty($turbo)) {
		$result = array('id'=>NULL,'status'=>'inactive');
	} else {
		foreach($turbo as $item) {
			if($item['answer']==0) $result=array('id'=>$item['idTurbo'],'status'=>'undone');
		}
		if(empty($result))  $result = array('id'=>$item['idTurbo'],'status'=>'done');
	}
	return $result;
}
function userLlifesaver($idUser,$rounds) {
	$lifesaver=array();
	krsort($rounds);
	foreach($rounds as $item) {
		if($item['status']=='undone') {
			$result2= $this->_CI->challenge_model->getSingleLifesaverResult('id',array('idUser'=>$idUser,'idRound'=>$item['idRound']));
			if(!empty($result2)) $item['status']='done';
			$save = array('id'=>$item['idRound'],'status'=>$item['status']);
			array_push($lifesaver,$save);
		}
	}
	if(empty($lifesaver)) {
		$result = array('id'=>NULL,'status'=>'inactive');
	} else {
		foreach($lifesaver as $item) {
			if($item['status']=='undone') $result=$item;
		}
		if(empty($result)) 	$result = array('id'=>NULL,'status'=>'done');
	}
	return $result;
}
function userRank($rank,$idUser) {
	$i=1;
	$position = '-';
	$pointsTop = 0;
	$behind=0;
	$points=0;
	foreach($rank as $item) {
		if($item['idUser']==$idUser) {
			$points = $item['points'];
			$position = $i;
			if($i>1) $behind = $pointsTop- $item['points'];
		}
		if($i==1) $pointsTop  =  $item['points'];
		$i++;
	}
	$result = array('position'=>$position,'behind'=>$behind,'pointsTop'=>$pointsTop-$points,'points'=>$points);
	return $result;
}
function userFinalRank($rank,$idUser) {
	$i=1;
	$position = '-';
	$pointsTop = 0;
	$behind=0;
	$points=0;
	foreach($rank as $item) {
		if($item['idUser']==$idUser) {
			$points = $item['points'];
			$position = $i;
			if($i>1) $behind = $pointsTop- $item['points'];
		}
		if($i==1) $pointsTop  =  $item['points'];
		$i++;
	}
	$result = array('position'=>$position,'behind'=>$behind,'pointsTop'=>$pointsTop-$points,'points'=>$points);
	return $result;
}
function myResults($rounds,$idUser,$lang) {
$id=(int)$id;
	$page = $this->user4->userKnowledge($idUser,$lang,$this->_edition,$id,$this->_actDate);
	$pageArr=array();
	foreach($page as $item) {
		$item['result']= $this->user4->userSingleResult($item['id'],$this->_user['idUser']);
		array_push($pageArr,$item);
	}
	/* kolo  ratunkowe */
	
	$lifesaver=array();
	foreach($rounds as $item) {
		if($item['status']=='undone') {
			$lifesaver[$item['idRound']] = array('id'=>$item['idRound'],'status'=>'undone');
		}
	}
	if(empty($lifesaver[$id])) {
		$lifesaverResult=array();
	} else {
		$lifesaverResult =$this->user4->userSingleLifesaverResult($id,$this->_user['idUser'],$this->_user['lang']);
	}
	$turbo=$this->challenge_model->getTurboAnswer('idTurbo,answer,points',array('idUser'=>$this->_user['idUser'],'edition'=>$this->_edition));
	krsort($turbo);

}
} /*koniec */