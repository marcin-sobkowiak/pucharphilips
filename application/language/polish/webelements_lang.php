<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang["web_here"] = "Jesteś tutaj";
$lang["web_remind"] = "Przypomnij hasło";
$lang["web_join"] = "Chcesz dołączyć do gry?";
$lang["web_register"] = "Zarejestruj się";
$lang["web_contact"] = "Bądźmy w kontakcie";
$lang["web_contact_answer"] = "Chętnie odpowiemy na nurtujące Cię pytania";
$lang["web_call"] = "Zadzwoń";
$lang["web_mobile_app"] = "Aplikacja mobilna";
$lang["web_start"] = "Wróć<br />na START";
$lang["web_contact_type"] = "Napisz do nas";
$lang["web_privacy"] = "Polityka Prywatności";


$lang["web_clock_to_start"] = "DO ROZPOCZĘCIA GRY POZOSTAŁO";
$lang["web_clock_to_end_knowledge"] = "TWÓJ CZAS NA PYTANIA";
$lang["web_clock_to_end_test"] = "TWÓJ CZAS NA TEST";
$lang["web_clock_to_days"] = "DNI";
$lang["web_clock_to_hours"] = "GODZ.";
$lang["web_clock_to_minutes"] = "MIN.";
$lang["web_clock_to_seconds"] = "SEK.";

$lang["web_login_panel"] = "Panel logowania";
$lang["subtitle"] = "Letnie Grand Prix 2020";

$lang["form_claim"] = "Chcesz dołączyć do GOOD GAME? Wypełnij poniższy formularz, a za moment otrzymasz mailem swoje dane dostępowe do gry treningowej GOODYEAR.";
$lang["form_remind"] = "Przypomnienie hasła";
$lang["form_user"] = "Użytkownik";
$lang["form_password"] = "Hasło";
$lang["form_btn_enter1"] = "ENTER";
$lang["form_btn_enter2"] = "THE GAME";
$lang["form_register"] = "Rejestracja";
$lang["form_userdata"] = "Dane uczestnika";
$lang["form_name"] = "Imię";
$lang["form_surname"] = "Nazwisko";
$lang["form_phone"] = "Telefon";
$lang["form_email"] = "E-mail";
$lang["form_serwis"] = "Serwis";
$lang["form_firmname"] = "Nazwa";
$lang["form_firmprofile"] = "Profil działalności";
$lang["form_adress"] = "Adres";
$lang["form_zip"] = "Kod pocztowy";
$lang["form_city"] = "Miejscowość";
$lang["form_ph"] = "Przedstawiciel handlowy Goodyear";
$lang["form_phName"] = "Imię i nazwisko";
$lang["form_agrees"] = "Zgody";
$lang["form_choose"] = "Wybierz";
$lang["form_profile1"] = "Dealer samochodowy";
$lang["form_profile2"] = "Serwis ogumienia";
$lang["form_profile3"] = "Przedstawiciel handlowy hurtowni";
$lang["form_agree1"] = 'Zapoznałem/am się i akceptuję zapisy <a href="/pl/about/rules" target="_blank">Regulaminu Konkursu</a> „Gra Szkoleniowa GOOD GAME”. Wyrażenie tej zgody jest dobrowolne, ale jej brak uniemożliwia rejestrację w Konkursie.';
$lang["form_agree1Short"] = 'Regulamin';
$lang["form_agreelabel1"] = "Wyrażam zgodę na przetwarzanie moich danych osobowych zgodnie z obowiązującymi przepisami prawa w celach komunikacji marketingowej, niezwiązanej z Konkursem następującymi kanałami:";
$lang["form_agree2"] = "Telefon";
$lang["form_agree3"] = "SMS";
$lang["form_agree4"] = "Poczta elektroniczna";
$lang["form_agreelabel2"] = "Wyrażenie tych zgód jest dobrowolne tzn. że nie stanowi warunku uczestnictwa w Konkursie.<br/>Zaznaczając którąkolwiek ze zgód potwierdza Pan/Pani swoje uprawnienie do reprezentowania firmy. Zgody mogą zostać wycofane w każdym czasie; wycofanie zgody numer 1 będzie jednak oznaczać zakończenie udziału firmy w Konkursie „Gra szkoleniowa GOOD GAME”. Szczegóły znajdziesz w Regulaminie Konkursu.";
$lang["form_btn_register"] = "Zarejestruj";
$lang["form_btn_send"] = "Wyślij";
$lang["form_btn_invite"] = "Zapraszam do gry";
$lang["remind_claim"] = "Nie pamiętasz hasła? To nie problem. Wpisz poniżej swój adres poczty elektronicznej podany podczas rejestracji i kliknij przycisk „Wyślij”. Otrzymasz wiadomość e-mail z przypomnieniem danych do logowania.";

$lang["menu_welcome"] = "Witaj";
$lang["menu_about_game"] = "O GRZE";
$lang["menu_start"] = "START";
$lang["menu_knowledge"] = "WIEDZA";
$lang["menu_questions"] = "PYTANIA I TESTY";
$lang["menu_additional_knowledge"] = "WIEDZA DODATKOWA";
$lang["menu_my_results"] = "MOJE WYNIKI";
$lang["menu_map"] = "MAPA GRY";
$lang["menu_faq"] = "FAQ";
$lang["menu_prizes"] = "NAGRODY";
$lang["menu_rules"] = "REGULAMIN";
$lang["menu_episode"] = "ODCINEK";
$lang["menu_answers"] = "ODPOWIEDZI";
$lang["menu_rank"] = "RANKING";
$lang["menu_account"] = "ZAPISZ GRACZA";
$lang["menu_archive"] = "ARCHIWUM";


$lang["side_act_circuit"] = "Aktualne Okrążenie";
$lang["side_position"] = "POZYCJA W RANKINGU";
$lang["side_level"] = "POZIOM";
$lang["side_levels"] = "Poziomy w grze";
$lang["side_sum_points"] = "SUMA PUNKTÓW";

$lang["box_knowledge"] = "Wiedza";
$lang["box_start_here"] = "Zacznij tutaj";
$lang["box_questions"] = "Pytania i Test";
$lang["box_check_your_knowledge"] = "Sprawdź swoją wiedzę";
$lang["box_my_results"] = "Moje Wyniki";
$lang["box_rank_and_help"] = "Ranking i koła ratunkowe";
$lang["box_map"] = "Mapa Gry";
$lang["box_how_to"] = "jak jechać, żeby wygrać";
$lang["box_circuit"] = "okrążenie";
$lang["box_my_circuit"] = "Moje okrążenia";
$lang["box_from"] = "od";
$lang["box_to"] = "do";
$lang["box_below"] = "poniżej";
$lang["box_points"] = "pkt.";
$lang["box_answers"] = "Odpowiedzi";
$lang["box_questions"] = "PYTANIA";
$lang["box_question_single"] = "Pytanie";
$lang["box_pills"] = "Pigułka wiedzy";
$lang["box_pills_archive"] = "Pigułka wiedzy - Archiwum";
$lang["box_once_again"] = "PRZECZYTAJ JESZCZE RAZ";
$lang["box_unread"] = "NIEPRZECZYTANE";
$lang["box_inactive"] = "NIEAKTYWNE";
$lang["box_active"] = "AKTYWNE";
$lang["box_undone"] = "NIEWYKONANE";
$lang["box_done"] = "WYKONANE";
$lang["box_claim_red"] = "Jeśli widzisz <strong>czerwony przycisk</strong>, to naciśnij go bez wahania.";
$lang["box_claim"] = "Jeśli zapoznałeś się z dostępną wiedzą, przejdź do zakładki";
$lang["box_question"] = "Test i pytania";
$lang["box_sum"] = "SUMA";
$lang["box_test"] = "TEST";
$lang["box_turbo"] = "TURBO";
$lang["box_help"] = "KOŁO RATUNKOWE";
$lang["box_premium"] = "PREMIA";
$lang["box_challenge"] = "Wyzwania na to okrążenie";
$lang["box_challenge2"] = "Powodzenia!";
$lang["box_max_points"] = "Maksymalna ilość pkt*";
$lang["box_claim_points"] = "Punktacja";
$lang["box_nothing"] = "BRAK";

$lang["legend_title"] = "Legenda";
$lang["legend_done"] = "Super, zadanie zrobione";
$lang["legend_todo"] = "Naciśnij bez wahania";
$lang["legend_inactive"] = "Jeszcze nieaktywne";

$lang["answer_correct"] = "POPRAWNA";
$lang["answer_incorrect"] = "NIEPOPRAWNA";
$lang["answer"] = "Odpowiedź";
$lang["answer_no"] = "BRAK odpowiedzi";
$lang["answer_txt1"] = "<strong>GOOD GAME to dobra gra</strong><br />- na wyższych poziomach gry dostajesz więcej punktów za te same działania.";
$lang["answer_txt2"] = "* Maksymalna ilość punktów w 1 okrążeniu, możliwa do uzyskania po udzieleniu prawidłowych odpowiedzi w 3 pytaniach i&nbsp;10 wyzwaniach testowych.<br /><strong>Powodzenia!</strong>";

$lang["rank_my_level"] = "Mój poziom";
$lang["rank_my_results"] = "Moje wyniki";
$lang["rank_my_position"] = "Moja pozycja w rankingu";
$lang["rank_my_position2"] = "Moja pozycja";
$lang["rank_ranking"] = "Ranking";
$lang["rank_ranking-final"] = "Ranking finałowy";
$lang["rank_my_points"] = "Moje punkty";
$lang["rank_behind_leader"] = "Strata do lidera";
$lang["rank_full_list"] = "Pełna lista zawodników";
$lang["rank_player"] = "Zawodnik";
$lang["rank_country"] = "Kraj";
$lang["rank_points"] = "Punkty";
$lang["rank_position"] = "Poz.";


$lang["btn_more"] = "więcej";
$lang["btn_less"] = "mniej";
$lang["btn_next"] = "Dalej";
$lang["btn_prev"] = "Wróć";
$lang["btn_correctness"] = "Poprawność";
$lang["btn_correct"] = "Dobrze";
$lang["btn_incorrect"] = "Źle";
$lang["btn_to_answer"] = "PRZEJDŹ DO PYTAŃ";
$lang["quiz_solved"] = "Dziękujemy za udział w wyzwaniu!";

$lang["status_next"] = "Wkrótce";
$lang["status_finished"] = "Ukończone";
$lang["status_act"] = "Aktualne";
$lang["status_omitted"] = "Pominięte";

$lang["popup_registerSuccess"] = "Witaj w GOOD GAME - zawalcz o dobrą pozycję rankingową. Twoje konto zostało utworzone. Na podany w formularzu adres e-mail wysłaliśmy dane dostępowe. Wejdź do GRY już dziś. Raz, dwa, trzy... START! ";
$lang["popup_succesRemind"] = "Na podany adres e-mail została wysłana wiadomość z hasłem.";
$lang["popup_errorRemind"] = "Nie ma takiego uczestnika.";
$lang["popup_dataUpdate"] = "Dziękujemy za aktualizację danych.";

/* account*/
$lang["yes"] = "Tak";
$lang["no"] = "Nie";
$lang["invite"] = "Zaproś";
$lang["invitePopup"] = "Czy wysłać ponowne zaproszenie do gry?";
$lang["level"] = "Poziom";
$lang["lastLogin"] = "Ostatnie logowanie"; /****/
$lang["addDate"] = "Data dodania"; 
$lang["usersPh"] = "Uczestnicy"; 
$lang["found"] = "Znaleziono"; 
$lang["starterLevel"] = "Ilu ma poziom STARTER"; 
$lang["championLevel"] = "Ilu ma poziom CHAMPION"; 
$lang["expertLevel"] = "Ilu ma poziom EXPERT"; 
$lang["loggedUsers"] = "Ilu zalogowało sie w grze"; 
$lang["registeredUsers"] = "Ilu przypisano do mojego konta"; 
$lang["invitedUsers"] = "Ilu zaprosiłem"; 
$lang["summary"] = "Podsumowanie"; 
$lang["yourMenu"] = "Twoje menu"; 
$lang["yourMenu1"] = "Zapisz nowego gracza"; 
$lang["yourMenu2"] = "Zaproszeni gracze"; 
$lang["popup_registerAccount"] = "Dziękujemy za wypełnienie formularza! Użytkownik został zarejestrowany. Na podany adres e-mail została wysłana wiadomość z danymi dostępowymi do Konkursu GOOD GAME."; 
$lang["popup_accountRemind"] = "Na podany adres e-mail wysłano ponownie zaproszenie."; 
$lang["lp"] = "L.p."; 
$lang["rank_excel"] = "Pozycja w rankingu ";
$lang["rank_excelFinal"] = "Pozycja w rankingu finałowym";
$lang["download"] = "Pobierz";

$lang["login_error"] = "Błędny login lub hasło.";




?>
