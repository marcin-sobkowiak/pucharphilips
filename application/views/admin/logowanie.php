<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Puchar Philips - administracja</title>

    <!-- Bootstrap Core CSS -->
    <link href="<? echo base_url() ?>css/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

   <!-- Custom fonts for this template-->
  <link href="<? echo base_url() ?>css/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<? echo base_url() ?>css/admin/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body>

<body class="bg-gradient-success">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block " style="background: #000 url(<? echo base_url()?>img/logo.png) 50% 50% no-repeat;"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Logowanie</h1>
                  </div>
		  <? if($error==2) {?><div class="alert alert-danger"><strong>BŁĄD!</strong> Konto zablokowane.</div><?}?>
		  <? if($error==1) {?><div class="alert alert-danger"><strong>BŁĄD!</strong> Błędne dane logowania.</div><?}?>
	  	<?php if(form_error('login')) {?><div class="alert alert-danger"><strong>BŁĄD!</strong>  Podaj login.</div><?}?>
	  	 <?php if(form_error('password')) {?><div class="alert alert-danger"><strong>BŁĄD!</strong>  Podaj hasło.</div><?}?>
                <? echo form_open() ?>
                    <div class="form-group">
			<input class="form-control" placeholder="login" name="login" type="text" value="">
                    </div>
                    <div class="form-group">
			<input class="form-control" placeholder="Password" name="password" type="password" value="">
                    </div>
		 <button class="btn btn-primary btn-user btn-block"> Zaloguj  </button>
                  </form>
                  <hr>
                  <div class="text-center">
		  <?/*<strong><a href="<? echo base_url()?>admin/remind">Przypomnij hasło</a></strong><br/>*/?>
		
                    <a class="small" href="<? echo base_url()?>privacy" target="_blank">Polityka prywatności</a>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

   

<!-- Bootstrap core JavaScript-->
  <script src="<? echo base_url() ?>css/admin/vendor/jquery/jquery.min.js"></script>
  <script src="<? echo base_url() ?>css/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<? echo base_url() ?>css/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<? echo base_url() ?>css/admin/js/sb-admin-2.min.js"></script>
<?/*
  <!-- Page level plugins -->
  <script src="<? echo base_url() ?>css/admin/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<? echo base_url() ?>css/admin/js/demo/chart-area-demo.js"></script>
  <script src="<? echo base_url() ?>css/admin/js/demo/chart-pie-demo.js"></script>
*/?>
<script>  
	$(document).ready(function() {	
setTimeout(function(){      
	$('.alert').fadeOut(1000)},2000); 
}); 

</script>
<link rel="stylesheet" href="<? echo base_url()?>cookies/divante.cookies.min.css" type="text/css">
<script type="text/javascript" src="<? echo base_url()?>cookies/divante.cookies.min.js"></script>
<script>window.jQuery.cookie || document.write('<script src="<? echo base_url()?>cookies/jquery.cookie.min.js"><\/script>')</script>
<script type="text/javascript">
        jQuery.divanteCookies.render({
            privacyPolicy : true,
            cookiesPageURL : ''
        });
</script>	
</body>

</html>

