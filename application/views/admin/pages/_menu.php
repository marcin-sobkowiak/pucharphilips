    <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<? echo base_url()?>admin_utilities">
        <div class="sidebar-brand-text mx-3"><img src="<? echo base_url()?>img/logo.png" style="width: 100%;"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
	<li class="nav-item active "><a class="nav-link" href="<? echo base_url() ?>admin_utilities"><i class="fas fa-cog"></i><span>Start</span></a></li>
	<li class="nav-item active">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="true" aria-controls="collapseUsers"><i class="fas fa-fw fa-users"></i><span>Uczestnicy</span></a>
		<div id="collapseUsers" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a class="collapse-item" href="<? echo base_url() ?>admin_users/newUsers"> Nie przypisani <br/>do rankingu</a>
				<a class="collapse-item" href="<? echo base_url() ?>admin_users">Wszyscy</a>
			</div>
		</div>
	</li>
	<li class="nav-item active">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="true" aria-controls="collapseUsers"><i class="fas fa-fw fa-users"></i><span>Pojedynki</span></a>
		<div id="collapseUsers" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a class="collapse-item" href="<? echo base_url() ?>admin_users/duels/wrzesien"> Wrzesień</a>
				<a class="collapse-item" href="<? echo base_url() ?>admin_users/duels/pazdziernik">Październik</a>
				<a class="collapse-item" href="<? echo base_url() ?>admin_users/duels/listopad">Listopad</a>
				<a class="collapse-item" href="<? echo base_url() ?>admin_users/duels/grudzien">Grudzień</a>
			</div>
		</div>
	</li>
	<li class="nav-item active"><a class="nav-link" href="<? echo base_url() ?>admin_reports"><i class="fas fa-file-excel"></i><span>Raporty</span></a></li>
<?/*        <div class="sidebar-heading text-primary">
		<div class="card">
			<div class="card-body">
			 <h5 class="card-title">Infolinia</h5>
			<i class="fas fa-phone-square-alt"></i>
				<span><a href="tel:+48801300303" class="text-reset">801 300 303</a><br/>(9:00-17:00)</span></a>
			 </div>
		</div> 
     </div>
 
<!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>
      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="utilities-color.html">Colors</a>
            <a class="collapse-item" href="utilities-border.html">Borders</a>
            <a class="collapse-item" href="utilities-animation.html">Animations</a>
            <a class="collapse-item" href="utilities-other.html">Other</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
*/?>
 <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
