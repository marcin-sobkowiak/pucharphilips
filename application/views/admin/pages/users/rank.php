﻿<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<? echo base_url()?>admin_utilities"><i class="fas fa-tachometer-alt"></i> Start</a></li>
		<li class="breadcrumb-item active"><i class="fas fa-list-ol"></i> Ranking</li>
	</ol>
</nav>
<div class="row">
	<div class="card-header">
	    <ul class="nav nav-pills card-header-pills">
	      <li class="nav-item">
		<a class="nav-link <? if($this->uri->segment(3)=='starter'||$this->uri->segment(3)==''){?>active<?}?>" href="<? echo base_url()?>admin_users/rank/starter">Starter</a>
	      </li>
	      <li class="nav-item">
		<a class="nav-link <? if($this->uri->segment(3)=='expert'){?>active<?}?>" href="<? echo base_url()?>admin_users/rank/expert">Expert</a>
	      </li>
	        <li class="nav-item">
		<a class="nav-link <? if($this->uri->segment(3)=='champion'){?>active<?}?>" href="<? echo base_url()?>admin_users/rank/champion">Champion</a>
	      </li>
	    </ul>
	</div>
	<div class="card shadow col-lg-12 border-bottom-primary">
		<div class="card-header"><h5 class="m-0 font-weight-bold text-primary">Ranking</h5></div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped list" >
							<thead>
							<tr>
							<th>Pozycja</th>
							<th>Uczestnik</th>
							<th>Punkty</th>
							<th></th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; foreach ($page as $item): ?> 
							<td><? echo $i ?></td>
							<td><? echo $item['name']?> <? echo $item['surname']?></td>
							<td><? echo $item['points']?></td>
							<td><a href="<? echo base_url()?>admin_users/edit/<? echo $item['idUser']?>" class="btn btn-warning btn-icon-split  btn-xs"><span class="icon text-white-50"><i class="fa fa-fw fa-info-circle"></i></span><span class="text"> Szczegóły uczestnika</span></a></td>
							</tr>
							<? $i++; endforeach ?>
							</tbody>
				</table>
			</div>	
		</div>	
	</div>	
</div>	