<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<? echo base_url()?>admin_utilities"><i class="fas fa-tachometer-alt"></i> Start</a></li>
		<li class="breadcrumb-item active"><i class="fas fa-list-ol"></i> Pojedynki - <? echo $month ?></li>
	</ol>
</nav>
<div class="row">

	<div class="card shadow col-lg-12 border-bottom-primary">
		<div class="card-header"><h5 class="m-0 font-weight-bold text-primary">Pojedynki - <? echo $month ?></h5></div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped list" >
							<thead>
							<tr>
							<th>Id gry</th>
							<th>Id Gracz 1</th>
							<th>Gracz 1</th>
							<th>Id Gracz 2</th>
							<th>Gracz 2</th>
							
							<th>Punkty 1 </th>
							<th>Punkty 2</th>
							<th>Zwyciezca</th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; foreach ($duels as $item): ?> 
							<tr>
							<td><? echo $item['id']?></td>
							<td><? echo $item['idPlayer1']?></td>
							<td><? echo $item['player1']?></td>
							<td><? echo $item['idPlayer2']?></td>
							<td><? echo $item['player2']?></td>
							<td><? echo $item['points1']?></td>
							<td><? echo $item['points2']?></td>
							<td><? echo $item['winner']?></td>
							</tr>
							<? $i++; endforeach ?>
							</tbody>
				</table>
			</div>	
		</div>	
	</div>	
</div>	