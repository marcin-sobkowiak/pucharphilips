﻿<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<? echo base_url()?>admin_utilities"><i class="fas fa-tachometer-alt"></i> Start</a></li>
		<li class="breadcrumb-item "><i class="fas fa-users"></i> Uczestnicy</li>
		<li class="breadcrumb-item active"><i class="fas fa-users"></i>  Szczegóły uczestnika <? echo $user['name']?> <? echo $user['surname']?></li>
		<li class="breadcrumb-item active"><i class="fas fa-info-circle"></i> Szczegóły okrążenia <? echo $idRound ?></li>
	</ol>
</nav>
<div class="card shadow border-bottom-primary mb-4">
		<div class="card-header"><h5 class="m-0 font-weight-bold text-primary">Dane podstawowe</h5></div>
			<div class="card-body">
				<div class="row">
					<div class="col-xl-12 col-md-3 mb-4">
								<div class="card-body">
									<table class="table table-bordered table-hover table-striped" >
										<tr><th>Uczestnik</th><th>Okrążenie</th><th>Język</th></tr>
										<tr>
											<td><? echo $user['name']?> <? echo $user['surname']?></td>
											<td><? echo $idRound ?></td>
											<td><? echo $user['lang'] ?></td>
										</tr>
									</table>
								</div>
							</div>
				</div>	
			</div>	
	</div>
<div class="row">
	<?/*
	 <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<a href="<? echo base_url()?>admin_users/add" class="btn btn-primary btn-icon-split  btn-xs"><span class="icon text-white-50"><i class="fa fa-fw fa-plus-circle"></i></span><span class="text"> Dodaj uczestnika</span></a>
	</div>
	*/?>
	<div class="card shadow col-lg-6 border-bottom-primary">
		<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Wiedza</h5></div>
		<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
							<tr>
							<th>Tytuł</th>
							<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; foreach($page as $item):?>
								<?if($item['type']=='wiedza') {?>
							<tr>
							<td><? echo $item['title'] ?></td>
							<td><span class="btn btn-xs <? if($item['status']=='read') {?>btn-success">Przeczytane<?} elseif($item['status']=='unread') {?>btn-danger">Nieprzeczytane<?} elseif($item['status']=='inactive') {?>btn-secondary">Nieaktywne<?}?></span></td>
							
							</tr>
							<?}?>
							<? $i++; endforeach?>        
							</tbody>
						</table>
					</div>	
		</div>	
	</div>	
	<div class="card shadow col-lg-6 border-bottom-primary">	
		<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Pytania</h5></div>
		<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
							<tr>
							<th>Tytuł</th>
							<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; $points=0; foreach($page as $item):?>
								<?if($item['type']=='wiedza') {?>
									<?  foreach($item['result'] as $item2):?>
										<tr>
										<td>Pytanie <? echo $i?></td>
										<td><? echo $item2['points']?> pkt</td>
										
										</tr>
									<? $i++;$points=$points+$item2['points']; endforeach   ?> 
								<?}?>
							<?  endforeach?> 
							<tr class="z-summary">
								<th>SUMA</th>
								<th><? echo $points ?> pkt</th>
							</tr>
							</tbody>
						</table>
					</div>	
		</div>		
	</div>
	<div class="card shadow col-lg-6 border-bottom-primary">	
		<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Test</h5></div>
		<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
							<tr>
							<th>Tytuł</th>
							<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; $points=0; foreach($page as $item):?>
								<?if($item['type']!='wiedza') {?>
									<?  foreach($item['result'] as $item2):?>
										<tr>
										<td>Pytanie <? echo $i?></td>
										<td><? echo $item2['points']?> pkt</td>
										
										</tr>
									<? $i++;$points=$points+$item2['points']; endforeach   ?> 
								<?}?>
							<?  endforeach?> 
							<tr class="z-summary">
								<th>SUMA</th>
								<th><? echo $item['sum'] ?> pkt</th>
							</tr>
							</tbody>
						</table>
					</div>	
		</div>
	</div>
	<div class="card shadow col-lg-6 border-bottom-primary">	
		<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Koło ratunkowe</h5></div>
		<div class="card-body">
		<? if(!empty($lifesaverResult)) {?>	 
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
							<tr>
							<th>Tytuł</th>
							<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; $points=0; foreach($lifesaverResult as $item):?>
								<?  foreach($item['result'] as $item2):?>
									<tr>
										<td>Pytanie <? echo $i?></td>
										<td><? echo $item2['points']?> pkt</td>
									</tr>
								<? $i++;$points=$points+$item2['points']; endforeach   ?> 
							<?  endforeach?> 
							<tr class="z-summary">
								<th>SUMA</th>
								<th><? echo $item['sum'] ?> pkt</th>
							</tr>
							</tbody>
						</table>
					</div>	
		<?} else {?>
			BRAK
		<?}?>
		</div>		
	</div>		
	<div class="card shadow col-lg-6 border-bottom-primary">	
		<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Premia Turbo</h5></div>
		<div class="card-body">
		<? if(!empty($turbo)) {?>	 
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
							<tr>
							<th>Tytuł</th>
							<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; $points=0; foreach($turbo as $item):?>
								<?  foreach($item['result'] as $item2):?>
									<tr>
										<td>Pytanie <? echo $i?></td>
										<td><? echo $item2['points']?> pkt</td>
									</tr>
								<? $i++;$points=$points+$item2['points']; endforeach   ?> 
							<?  endforeach?> 
							<tr class="z-summary">
								<th>SUMA</th>
								<th><? echo $points ?> pkt</th>
							</tr>
							</tbody>
						</table>
					</div>	
		<?} else {?>
			BRAK
		<?}?>
		</div>		
	</div>		
</div>	