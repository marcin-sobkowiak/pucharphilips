﻿<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<? echo base_url()?>admin_utilities"><i class="fas fa-tachometer-alt"></i> Start</a></li>
		<li class="breadcrumb-item "><i class="fas fa-users"></i> Uczestnicy</li>
		<li class="breadcrumb-item active"><i class="fas fa-users"></i> Wyszukiwanie</li>
	</ol>
</nav>

<div class="row">
	<?/*
	 <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<a href="<? echo base_url()?>admin_users/add" class="btn btn-primary btn-icon-split  btn-xs"><span class="icon text-white-50"><i class="fa fa-fw fa-plus-circle"></i></span><span class="text"> Dodaj uczestnika</span></a>
	</div>
	*/?>
	<div class="card shadow col-lg-12 border-bottom-primary">
		<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Uczestnicy</h5></div>
		<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped list" id="dataTables-example">
							<thead>
							<tr>
							<th>Status</th>
							<th>WW90</th>
							<th>Login</th>
							<th>Imię</th>
							<th>E-mail</th>
							<th>Telefon komórkowy</th>
							<th>Data rejestracjia</th>
							<th>Spółka</th>
							<th></th>
							</tr>
							</thead>
							<tbody>
							<? $i=1; foreach ($page as $item): ?> 
							<tr>
							<td><? echo $item['status'] ?></td>
							<td><? echo $item['clientNumber'] ?></td>
							<td><? echo $item['login'] ?></td>
							<td><? echo $item['name'] ?> </td>
							<td><? echo $item['email'] ?></td>
							<td><? echo $item['phone'] ?></td>
							<td><? echo $item['registerDate'] ?></td>
							<td><? echo $this->_firms[$item['firm']]['title'] ?></td>
							<td><a href="<? echo base_url()?><? echo $this->_program?>/admin_users/edit/<? echo $item['id']?>" class="btn btn-warning btn-icon-split  btn-xs" target="_blank" ><span class="icon text-white-50"><i class="fa fa-fw fa-info-circle"></i> </span><span class="text">Szczegóły</span></a></td>
							</tr>
							<? $i++; endforeach ?>
							</tbody>
						</table>
					</div>	
		</div>	
			
	</div>	
</div>	