﻿<?
$disabled='readonly';
if($this->_admin['adminType']=='god') $disabled='';
?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<? echo base_url()?>admin_utilities"><i class="fas fa-tachometer-alt"></i> Start</a></li>
		<li class="breadcrumb-item "><i class="fas fa-users"></i> Uczestnicy</li>
		<li class="breadcrumb-item "><a href="<? echo base_url()?>admin_users"><i class="fas fa-users"></i> Wszyscy</a></li>
		<li class="breadcrumb-item active"><i class="fas fa-info-circle"></i>  Szczegóły uczestnika</li>
	</ol>
</nav>
<? if(validation_errors()!='') {?>
<div class="alert alert-danger"><? echo validation_errors()?></div>
<?}?>
<? if($this->_admin['adminType']=='god') {?><form action="" method="post"><?}?>
<div class="row">	
		<div class=" col-lg-6  mb-4">
			<div class="card shadow border-bottom-primary">
				<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Dane zespołu</h5></div>
				<div class="card-body ">
					<div class="table-responsive">
						<table class="table table-hover">
							<tr><td>Data rejestracji:</td><td><? if($page['registerDate']) {?><? echo $page['registerDate']?><?} else {?>Edycja Zima 2020<?}?></td></tr>			
							<tr><td>Ostatnie logowanie www:</td><td><? echo $page['lastLogin']?></td></tr>			
							<tr><td>Hasło:</td><td><? echo $page['password']?></td></tr>			
							<? if($this->_admin['adminType']=='god') {?>	
								<tr ><td colspan="2"><div class="checkbox"><label><input type="checkbox"  <? echo $disabled ?> name="active" value="1" <? if($page['active']==1) {?>checked="checked"<?}?>  <? if($this->_admin['adminType']!='god') {?>readonly<?}?> > Aktywny</label></td></tr>
							<?}?>	
							<tr><td>region rankingowy:</td><td>
								<select class="form-control" name="idRank"  <? echo $disabled ?>  >
									<option value="NULL"></option>
										<option value="1" <? if(1==$page['idRank']) {?>selected<?}?> ><? echo $this->_ph[1]?></option>
										<option value="2" <? if(2==$page['idRank']) {?>selected<?}?> ><? echo $this->_ph[2]?></option>
										<option value="3" <? if(3==$page['idRank']) {?>selected<?}?> ><? echo $this->_ph[3]?></option>
										<option value="4" <? if(4==$page['idRank']) {?>selected<?}?> ><? echo $this->_ph[4]?></option>
										<option value="5" <? if(5==$page['idRank']) {?>selected<?}?> ><? echo $this->_ph[5]?></option>
										<option value="6" <? if(6==$page['idRank']) {?>selected<?}?> ><? echo $this->_ph[6]?></option>
										<option value="7" <? if(7==$page['idRank']) {?>selected<?}?> ><? echo $this->_ph[7]?></option>>
								</select>
							</td></tr>	
							<tr><td>Nazwa drużyny:</td><td><input <? echo $disabled ?> type="text" name="login" class="form-control <?php if(form_error('login')) {?> is-invalid<?}?>"  value="<? if(set_value('login')) { if($page['login']!=set_value('login')) { echo set_value('login'); } else {  echo $page['login'];  } } else {echo $page['login']; }?>"    /></td></tr>	
							<tr><td>Dystrybutor:</td><td><input <? echo $disabled ?> type="text" name="firm" class="form-control <?php if(form_error('firm')) {?> is-invalid<?}?>"  value="<? if(set_value('firm')) { if($page['firm']!=set_value('firm')) { echo set_value('firm'); } else {  echo $page['firm'];  } } else {echo $page['firm']; }?>"    /></td></tr>	
							<tr><td>NIP:</td><td><input <? echo $disabled ?> type="text" name="nip" class="form-control nip <?php if(form_error('nip')) {?> is-invalid<?}?>"  value="<? if(set_value('nip')) { if($page['nip']!=set_value('nip')) { echo set_value('nip'); } else {  echo $page['nip'];  } } else {echo $page['nip']; }?>"    /></td></tr>	
							<tr><td>Ulica:</td><td><input <? echo $disabled ?> type="text" name="street" class="form-control <?php if(form_error('street')) {?> is-invalid<?}?>"  value="<? if(set_value('street')) { if($page['street']!=set_value('street')) { echo set_value('street'); } else {  echo $page['street'];  } } else {echo $page['street']; }?>"    /></td></tr>	
							<tr><td>Nr domu:</td><td><input <? echo $disabled ?> type="text" name="home" class="form-control <?php if(form_error('home')) {?> is-invalid<?}?>"  value="<? if(set_value('home')) { if($page['home']!=set_value('home')) { echo set_value('home'); } else {  echo $page['home'];  } } else {echo $page['home']; }?>"    /></td></tr>	
							<tr><td>Nr lokalu:</td><td><input <? echo $disabled ?> type="text" name="number" class="form-control <?php if(form_error('number')) {?> is-invalid<?}?>"  value="<? if(set_value('number')) { if($page['number']!=set_value('number')) { echo set_value('number'); } else {  echo $page['number'];  } } else {echo $page['number']; }?>"    /></td></tr>	
							<tr><td>Kod pocztowy:</td><td><input <? echo $disabled ?> type="text" name="zip" class="form-control zip<?php if(form_error('zip')) {?> is-invalid<?}?>"  value="<? if(set_value('zip')) { if($page['zip']!=set_value('zip')) { echo set_value('zip'); } else {  echo $page['zip'];  } } else {echo $page['zip']; }?>"    /></td></tr>	
							<tr><td>Miejscowość:</td><td><input <? echo $disabled ?> type="text" name="city" class="form-control <?php if(form_error('city')) {?> is-invalid<?}?>"  value="<? if(set_value('city')) { if($page['city']!=set_value('city')) { echo set_value('city'); } else {  echo $page['city'];  } } else {echo $page['city']; }?>"    /></td></tr>								
							<tr><td>E-mail:</td><td><input <? echo $disabled ?> type="text"  required name="email" class="form-control <?php if(form_error('email')) {?> is-invalid<?}?>"  value="<? if(set_value('email')) { if($page['email']!=set_value('email')) { echo set_value('email'); } else {  echo $page['email'];  } } else {echo $page['email']; }?>"    /></td></tr>	
							<tr><td>Telefon komórkowy:</td><td><input <? echo $disabled ?> type="text"  required name="phone"  class="form-control phone <?php if(form_error('phone')) {?> is-invalid<?}?>" value="<? if(set_value('phone')) { if($page['phone']!=set_value('phone')) { echo set_value('phone'); } else {  echo $page['phone'];  } } else {echo $page['phone']; }?>"  /> </td></tr>		
							
	
							
						
						<tr ><th colspan="2">Zgody</th></tr>
						<tr ><td colspan="2"><div class="checkbox"><label><input type="checkbox"  disabled  name="agree1" value="1" <? if($page['agree1']==1) {?>checked="checked"<?}?>  <? if($this->_admin['adminType']!='god') {?>readonly<?}?> >  Oświadczam, że zgłaszam udział Drużyny ze wskazanej hurtowni w Konkursie „Puchar Philips” przy wykorzystaniu podanych wyżej danych, a także, że cała Drużyna zapoznała się i akceptuje regulamin tego Konkursu.</label></td></tr>
						<tr ><td colspan="2"><div class="checkbox"><label><input type="checkbox"  disabled name="agree2" value="1" <? if($page['agree2']==1) {?>checked="checked"<?}?>  <? if($this->_admin['adminType']!='god') {?>readonly<?}?> > Oświadczam, że uzyskałem/uzyskałam zgodę od pracodawcy (właściciela hurtowni) na swój udział oraz udział pozostałych członków drużyny w Konkursie „Puchar Philips”.</label></td></tr>
						<tr ><td colspan="2"><div class="checkbox"><label><input type="checkbox"  disabled name="agree3" value="1" <? if($page['agree3']==1) {?>checked="checked"<?}?>  <? if($this->_admin['adminType']!='god') {?>readonly<?}?> > Chcę otrzymywać spersonalizowane wiadomości o charakterze promocyjnym na temat produktów, usług, wydarzeń i promocji, dotyczące marek powiązanych z Signify. Dowiedz się więcej</label></td></tr>
						<tr ><td colspan="2"><div class="checkbox"><label><input type="checkbox"  disabled name="agree4" value="1" <? if($page['agree4']==1) {?>checked="checked"<?}?>  <? if($this->_admin['adminType']!='god') {?>readonly<?}?> > Akceptuję niniejsze Zasady korzystania Wszystkie dane osobowe będą przetwarzane zgodnie z niniejszymi Polityka prywatności *</label></td></tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class=" col-lg-6  mb-4 ">
			<div class="card shadow border-bottom-primary mb-4">
				<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Członkowie drużyny </h5></div>
				<div class="card-body ">
					<div class="table-responsive">
						<table class="table table-hover">
							<tr><th colspan="2">Kapitan</th></tr>
							<tr><td>Imię:</td><td><input <? echo $disabled ?> type="text" required name="name1" class="form-control <?php if(form_error('name1')) {?> is-invalid<?}?>"  value="<? if(set_value('name1')) { if($page['name1']!=set_value('name1')) { echo set_value('name1'); } else {  echo $page['name1'];  } } else {echo $page['name1']; }?>"    /></td></tr>	
							<tr><td>Nazwisko:</td><td><input <? echo $disabled ?> type="text" required name="surname1" class="form-control <?php if(form_error('surname1')) {?> is-invalid<?}?>"  value="<? if(set_value('surname1')) { if($page['surname1']!=set_value('surname1')) { echo set_value('surname1'); } else {  echo $page['surname1'];  } } else {echo $page['surname1']; }?>"    /></td></tr>	
							<tr><th colspan="2">Zawodnik 1 </th></tr>
							<tr><td>Imię:</td><td><input <? echo $disabled ?> type="text" required name="name2" class="form-control <?php if(form_error('name2')) {?> is-invalid<?}?>"  value="<? if(set_value('name2')) { if($page['name2']!=set_value('name2')) { echo set_value('name2'); } else {  echo $page['name2'];  } } else {echo $page['name2']; }?>"    /></td></tr>	
							<tr><td>Nazwisko:</td><td><input <? echo $disabled ?> type="text" required name="surname2" class="form-control <?php if(form_error('surname2')) {?> is-invalid<?}?>"  value="<? if(set_value('surname2')) { if($page['surname2']!=set_value('surname2')) { echo set_value('surname2'); } else {  echo $page['surname2'];  } } else {echo $page['surname2']; }?>"    /></td></tr>	
							<tr><th colspan="2">Zawodnik 2</th></tr>
							<tr><td>Imię:</td><td><input <? echo $disabled ?> type="text" required name="name3" class="form-control <?php if(form_error('name3')) {?> is-invalid<?}?>"  value="<? if(set_value('name3')) { if($page['name3']!=set_value('name3')) { echo set_value('name3'); } else {  echo $page['name3'];  } } else {echo $page['name3']; }?>"    /></td></tr>	
							<tr><td>Nazwisko:</td><td><input <? echo $disabled ?> type="text" required name="surname3" class="form-control <?php if(form_error('surname3')) {?> is-invalid<?}?>"  value="<? if(set_value('surname3')) { if($page['surname3']!=set_value('surname3')) { echo set_value('surname3'); } else {  echo $page['surname3'];  } } else {echo $page['surname3']; }?>"    /></td></tr>	
						</table>
					</div>
				</div>
			</div>
		</div>
</div>	
</div class="row">
<div class=" col-lg-12  mb-4 ">
<div class="card shadow border-bottom-primary mb-4">
	<div class="card-body ">
		<div class="table-responsive">
						<? if($this->_admin['adminType']=='god') {?>
						<input type="hidden" name="idUser" value="<? echo $page['id']?>">
						<button class="btn  btn-primary">Zapisz</button>
						</form>
						<?}?>
		</div>
	</div>
</div>
</div>
</div>
<? if($this->_admin['adminType']=='god') {?></form><?}?>
<script type="text/javascript" src="<? echo base_url()?>scripts/js/jquery.maskedinput.js"></script>    
<script>
$(document).ready(function() {    

	$(".phone").mask("999999999");
	$(".nip").mask("9999999999");
	$(".zip").mask("99-999");

});
</script>