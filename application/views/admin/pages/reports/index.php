<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<? echo base_url()?>admin_utilities"><i class="fas fa-tachometer-alt"></i> Start</a></li>
		<li class="breadcrumb-item active">  <i class="fas fa-file-excel"></i> Raporty</li>
	</ol>
</nav>
<div class="row">
	<div class=" col-lg-4 mb-4">
		<div class="card shadow border-bottom-primary">
			<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Uczestnicy</h5></div>
				<div class="card-body">
					<div class="table-responsive">
					<form action="<? echo base_url()?>admin_download/users" method="post"><input type="hidden" name="trigger" value="1">
						<table class="table table-hover">	
							<tr><td colspan="2"><button  class="btn btn-info btn-icon-split  btn-xs"><span class="icon text-white-50"><i class="fas fa-download"></i> </span><span class="text">Pobierz raport</button></td></tr>
						</table>
					</form>	
					</div>
				</div>
		</div>
	</div>
	<div class=" col-lg-4 mb-4">
		<div class="card shadow border-bottom-primary">
			<div class="card-header">  <h5 class="m-0 font-weight-bold text-primary">Wyniki</h5></div>
				<div class="card-body">
					<div class="table-responsive">
					<form action="<? echo base_url()?>admin_download/results" method="post"><input type="hidden" name="trigger" value="1">
						<table class="table table-hover">	
							<tr><td colspan="2"><button  class="btn btn-info btn-icon-split  btn-xs"><span class="icon text-white-50"><i class="fas fa-download"></i> </span><span class="text">Pobierz raport</button></td></tr>
						</table>
					</form>	
					</div>
				</div>
		</div>
	</div>
</div>

