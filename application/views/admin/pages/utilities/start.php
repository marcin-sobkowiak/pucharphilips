<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active"><i class="fas fa-tachometer-alt"></i> Start</li>
	</ol>
</nav>
<div class="row">
<? if($this->_admin['adminType']=='god') {?>
	<div class="col-xl-3 col-md-6 mb-4">
	<a href="<? echo base_url()?>admin_users/newUsers">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Uczestnicy nie przypisani do rankingu</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-fw fa-users"></i><span class="badge <? if($users) {?>badge-danger<?} else {?>badge-success<?}?> badge-counter"><? echo $users?></span>
					</div>
				</div>
			</div>
		</div>
	</a>
	</div>
<?}?>	

</div>

