﻿	<ol class="breadcrumb">
                   <li><a href="<?php  echo base_url() ?>admin_utilities"><i class="fa fa-fw fa-dashboard"></i> Start</a></li>
                    <li class="active"><i class="fa fa-fw fa-users"></i> Lista adminów</li>
          </ol>
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><?php  echo $title?></div>
                        <div class="panel-body">
			<a href="<?php  echo base_url()?>admin_utilities/add" class="btn btn-success"><i class="fa fa-plus-circle fa-fw"></i> Dodaj nowego admina</a>
                            <div class="dataTable_wrapper">
			     <table class="table table-striped table-bordered table-hover list">
					<thead><tr>
					<th>Lp.</th>
					<th>Login</th>
					<th>Ostatnie logowanie</th>
					<th>Status</th>
					<th>Typ</th>
					<th></th>
					</tr></thead><tbody>
					<?php  $i=1;  foreach($admins as $item):?>
						<tr>
						    <td><?php  echo $i?></td>
						    <td><?php  echo $item['login']?> </a></td>
						   <td><?php  echo $item['lastLogin']?></td>
						   <td> <?php  if($item['active']==0) {?><span class="btn btn-danger btn-xs"  style="cursor:default"><i class="fa  fa-power-off fa-fw"></i> Nieaktywny<?php } else{?>
						   <span class="btn btn-success btn-xs"  style="cursor:default"><i class="fa  fa-power-off fa-fw"></i> Aktywny<?php }?></span></td>
						     <td> <?php  if($item['type']==1) {?>Admin główny<?php } else{?>Użytkownik  <?php }?></td>
						   <td><a href="<?php  echo base_url()?>admin_utilities/edit/<?php  echo $item['idAdmin']?>" class="btn btn-warning btn-xs"><i class="fa   fa-edit fa-fw"></i>  Edycja</a></td>
						  </tr>
						<?php  $i++; endforeach?>
					    </tbody>
					</table>
			  </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>										
