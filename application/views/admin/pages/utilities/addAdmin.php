﻿	<ol class="breadcrumb">
                   <li><a href="<?php  echo base_url() ?>admin_utilities"><i class="fa fa-fw fa-dashboard"></i> Start</a></li>
                    <li ><a href="<?php  echo base_url() ?>admin_utilities/admin"><i class="fa fa-fw fa-users"></i> Lista adminów</a></li>
                    <li class="active"><i class="fa fa-fw fa-plus-circle"></i> Dodawanie</li>
          </ol>
<?php  if(form_error('name')) {?><div class="alert alert-danger">Nie imienia i nazwiskka.</div><?php }?>
			<?php  if(form_error('login')) {?><div class="alert alert-danger">Nie loginu.</div><?php }?>
			<?php  if(form_error('password')) {?><div class="alert alert-danger">Nie podałeś poprawnego hasła.</div><?php }?>
<form method="post" action="" enctype="multipart/form-data" name="Form1">
<div class="row">
	<div class="col-lg-6">
                    <div class="panel panel-primary">
			<div class="panel-heading"><?php  echo $title?></div>
				<div class="panel-body">
					<div class="table-responsive">

							<table class="table table-striped ">	
							<tr><td>Typ</td><td><select class="form-control" name="type" >
							<option value="1" <?php  if(set_value('type')==1) {?>selected="selected"<?php }?>>Admin główny</option>
							</select></td></tr>							
							<tr><td>Status</td><td><select class="form-control" name="active" >
							<option value="0" <?php  if(set_value('active')==0) {?>selected="selected"<?php }?>>Nieaktywny</option>
							<option value="1" <?php  if(set_value('active')==1) {?>selected="selected"<?php }?>>Aktywny</option>
							</select></td></tr>
							<tr><td>Imię i nazwisko</td><td><input type="text" name="name" value="<?php  echo set_value('name')?>" class="form-control"></td></tr>
							<tr><td>Login <br/>(min 3 znaki)</td><td><input type="text" name="login" value="<?php  echo set_value('login')?>" class="form-control"></td></tr>
							<tr><td>Hasło <br/>(min 6 znaków)</td><td><input type="text" name="password" value="" class="form-control"></td></tr>
							<tr><td></td><td><button type="submit" class="btn btn-primary"   style="float: right;" ><i class="fa   fa-save fa-fw"></i> Zapisz</button></td></tr>
							</table>
					</div>
				</div>
		</div>
	</div>
	
</div>	
</form>
