	<ol class="breadcrumb">
                   <li><a href="<? echo base_url() ?>admin_utilities"><i class="fa fa-fw fa-dashboard"></i> Start</a></li>
                    <li class="active"><i class="fa fa-fw fa-gear"></i> Zmiana hasła</li>
          </ol>
<? if(form_error('nowe1')) {?><div class="alert alert-danger"><? echo form_error('nowe1')?></div><?}?>
<? if(form_error('nowe2')) {?><div class="alert alert-danger"><? echo form_error('nowe2')?></div><?}?>
<? if(form_error('password')) {?><div class="alert alert-danger"><? echo form_error('password')?></div><?}?>
<div class="row">
	<div class="col-lg-6">
                    <div class="panel panel-primary">
			<div class="panel-heading">
			   Zmiana hasła
			</div>
				<div class="panel-body">
					<div class="table-responsive">			
						<form  method="post" action="<? echo base_url()?>admin_utilities/password"  >
						 <table class="table table-striped ">
							<tr><th>stare hasło:</th><td><input type="password" name="stare" value="" class="form-control">   <p class="help-block">minimum 6 znaków</p></td></tr>
							<tr><th>nowe hasło*:</th><td><input type="password" name="nowe1" value="" class="form-control"></td></tr>
							<tr><th>powtórz hasło:</th><td><input type="password" name="nowe2" value="" class="form-control"></td></tr>
							<tr><th></th><td><button type="submit" class="btn btn-primary">Zmień hasło</button></td></tr>
							</table>

						</form>
					</div>
				</div>
		</div>
	</div>
</div>	
	
     