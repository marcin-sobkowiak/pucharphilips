<!DOCTYPE html>
<html lang="en">

<head>

     <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

   <title>Puchar Philips - administracja</title>
  <!-- Bootstrap Core CSS -->
    <link href="<? echo base_url() ?>css/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

   <!-- Custom fonts for this template-->
  <link href="<? echo base_url() ?>css/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<? echo base_url() ?>css/admin/css/sb-admin-2.min.css" rel="stylesheet">
  
  <!-- Custom styles for this page -->
  <link href="<? echo base_url() ?>css/admin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript-->
  <script src="<? echo base_url() ?>css/admin/vendor/jquery/jquery.min.js"></script>
</head>

<body>
   <!-- Page Wrapper -->
  <div id="wrapper">
      <!-- Sidebar -->
	
	<? include('pages/_menu.php') ?>
	
      <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
	<!-- Topbar -->
		<? include('pages/_menuTop.php') ?>
             <!-- End of Topbar -->

<!-- Begin Page Content -->
        <div class="container-fluid">
	<? if($this->session->flashdata('update')) {?><div class="alert alert-success"><? echo $this->session->flashdata('update')?>.</div><?}?>
	<? if($this->session->flashdata('error')) {?><div class="alert alert-danger"><strong>BŁĄD!</strong>  <? echo $this->session->flashdata('error')?>.</div><?}?>
	
	<? include('pages/'.$pageFile) ?>
	
	</div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
<?/*  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Wylogować ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">NIE</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

*/?>


    <!-- Bootstrap core JavaScript-->

  <script src="<? echo base_url() ?>css/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<? echo base_url() ?>css/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<? echo base_url() ?>css/admin/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<? echo base_url() ?>css/admin/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<? echo base_url() ?>css/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<!-- z panelu 4A-->
<script src="<? echo base_url() ?>scripts/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="<? echo base_url() ?>scripts/themes/base/jquery.ui.all.css">
<script>
/*** Handle jQuery plugin naming conflict between jQuery UI and Bootstrap ***/
$.widget.bridge('uibutton', $.ui.button);
$.widget.bridge('uitooltip', $.ui.tooltip);
</script>

<script type="text/javascript">
$(document).ready(function() {
 $('.list').dataTable( {
        "aaSorting": [[ 0, "asc" ]],
	"bStateSave": true,
	 language: {
	  "sProcessing":   "Proszę czekać...",
	    "sLengthMenu":   "Pokaż _MENU_ pozycji",
	    "sZeroRecords":  "Brak pozycji do wyświetlenia",
	    "sInfo":         "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
	    "sInfoEmpty":    "Pozycji 0 z 0 dostępnych",
	    "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
	    "sInfoPostFix":  "",
	    "search":       "Szukaj:",
	    "sUrl":          "",
		    "oPaginate": {
		    "sFirst":    "Pierwsza",
		    "sPrevious": "Poprzednia",
		    "sNext":     "Następna",
		    "sLast":     "Ostatnia"
		    }
	}    
    } );
   $(".datepicker").datepicker({
		showOn: 'focus',
		dateFormat: 'yy-mm-dd',
		buttonImageOnly: true,
		dayNamesMin: ['N','Pn','Wt','Śr','Cz','Pt','So'],
		monthNames: ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec',
                'Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
                monthNamesShort: ['Sty','Lu','Mar','Kw','Maj','Cze',
                'Lip','Sie','Wrz','Pa','Lis','Gru'],		
	}); 
   $('input[name="chkall"]').click(function() {
                $(":checkbox").attr('checked', $(this).attr('checked'));
        });	
    setTimeout(function(){      
	$('.alert').fadeOut(1000)},2000); 
			
});
</script>
<!-- include summernote css/js -->
<link href="<? echo base_url()?>scripts/summernote/summernote.css" rel="stylesheet">
<script src="<? echo base_url()?>scripts/summernote/summernote.js"></script>
  <script>
    $(document).ready(function() {
		$('.summernote').summernote({
		 height: 300,    
		 codeviewIframeFilter: true,
		 dialogsInBody: true,
		  callbacks: {
			onPaste: function (e) {
			    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
			    e.preventDefault();
			    document.execCommand('insertText', false, bufferText);
			}
		    },
		 toolbar: [
		    // [groupName, [list of button]]
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['font', ['strikethrough', 'superscript', 'subscript']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['style','ul', 'ol', 'paragraph']],
		    ['insert', ['link', 'picture',]],
		    ['view', ['codeview']],
		  ]
		
		});
    });
  </script>
</body>

</html>
