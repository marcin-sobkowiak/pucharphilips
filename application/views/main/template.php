<!DOCTYPE html>
<html lang="pl-PL">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /> 
  
    <meta name="Description" content="" />
    <meta name="Keywords" content="" />
    <meta name="Author" content="" />

    <title>Philips</title>

	<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
	<link href="<? echo base_url()?>favicon.ico" type="image/x-icon" rel="icon" />

	<link rel="stylesheet" href="<? echo base_url()?>css/bootstrap.min.css" type="text/css" />      
	<link rel="stylesheet" href="<? echo base_url()?>css/jquery.bxslider.min.css" type="text/css" />
	<link rel="stylesheet" href="<? echo base_url()?>css/chosen.css" type="text/css" />
	<link rel="stylesheet" href="<? echo base_url()?>css/hamburgers.css" type="text/css" />
	<link rel="stylesheet" href="<? echo base_url()?>css/style.css?v=2" type="text/css" />	
	
	<script src="<? echo base_url()?>scripts/js/jquery-3.3.1.min.js"></script>
	<script src="<? echo base_url()?>scripts/js/jquery.bxslider.min.js"></script>
	<script src="<? echo base_url()?>scripts/js/popper.min.js"></script>
	<script src="<? echo base_url()?>scripts/js/bootstrap.min.js"></script>
	<script src="<? echo base_url()?>scripts/js/chosen.jquery.js"></script>
	<script src="<? echo base_url()?>scripts/js/placeholders.min.js"></script>
	<script src="<? echo base_url()?>scripts/js/icheck.min.js"></script>
	<script src="<? echo base_url()?>scripts/js/jquery.hoverIntent.js"></script>
	<script src="<? echo base_url()?>scripts/js/script.js"></script> 
    <!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-59056447-49"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-59056447-49');
</script>
</head>
<body class="<? echo $classBg?>">
	<? if($this->session->flashdata('popup')||validation_errors()!='') { ?>
	<script>
		$(function(){
			$('#myModal').modal('show');
		});
	</script>
	
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
		  <div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
				</div>
				<div class="modal-body">
					<div class="txt">
						<? echo $this->session->flashdata('popup') ?>
						<? echo validation_errors() ?>
					</div>
				</div>
		  </div>
		</div>
	</div>	       
          <?}?> 
	<header>
		<div class="header-box">
			<div class="container">
				<div class="h-box-left">
				    <a href="<? echo base_url()?>" class="logo"><img src="<? echo base_url()?>img/logo.png" alt="" /></a>
				</div>
				<nav class="main-menu">
					<ul>
						<? if(!$this->_user['logged']) {?>
							<li <? if($this->uri->segment(1)=='logowanie') {?>class="current"<?}?>><a href="<? echo base_url()?>logowanie">LOGIN</a></li>
							<li <? if($this->uri->segment(1)=='zasady') {?>class="current"<?}?>><a href="<? echo base_url()?>zasady">ZASADY</a></li>
							<li <? if($this->uri->segment(1)=='nagrody') {?>class="current"<?}?>><a href="<? echo base_url()?>nagrody">NAGRODY</a></li>
							<li <? if($this->uri->segment(1)=='kontakt') {?>class="current"<?}?>><a href="<? echo base_url()?>kontakt">KONTAKT</a></li>
						<?} else {?>
							<li <? if($this->uri->segment(1)=='zasady') {?>class="current"<?}?>><a href="<? echo base_url()?>zasady">ZASADY</a></li>
							<li <? if($this->uri->segment(1)=='nagrody') {?>class="current"<?}?>><a href="<? echo base_url()?>nagrody">NAGRODY</a></li>
							<li <? if($this->uri->segment(1)=='ranking'||$this->uri->segment(1)=='') {?>class="current"<?}?>><a href="<? echo base_url()?>ranking">RANKING</a></li>
							<li <? if($this->uri->segment(1)=='pojedynki') {?>class="current"<?}?>><a href="<? echo base_url()?>pojedynki">POJEDYNKI</a></li>
							<li <? if($this->uri->segment(1)=='misje-specjalne') {?>class="current"<?}?>><a href="<? echo base_url()?>misje-specjalne">MISJE SPECJALNE</a></li>
							<li <? if($this->uri->segment(1)=='kontakt') {?>class="current"<?}?>><a href="<? echo base_url()?>kontakt">KONTAKT</a></li>
						<?}?>
					</ul>
				</nav>                
				<div class="h-box-right">
					<? if($this->_user['logged']) {?>
					<div class="dropdown">
						<a class="btn-1 dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><? echo $this->_user['login']?></a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
							<a class="dropdown-item" href="<? echo base_url()?>zmiana-hasla">Zmiana hasła</a>
							<a class="dropdown-item" href="<? echo base_url()?>wyloguj">Wyloguj</a>
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</div>
	</header>
	<? include($pageFile)?>
    	<footer>
		<div class="container">
            <nav class="f-menu">
                <ul>
                    <li><a href="<? echo base_url()?>files/polityka_prywatnosci.pdf" target="_blank">Polityka prywatności</a></li>
                    <li><a href="<? echo base_url()?>files/zasad_przetwarzania.pdf" target="_blank">Zasady przetwarzania danych osobowych</a></li>
                </ul>
            </nav>
            <div class="f-logo"><img src="<? echo base_url()?>img/philips.png" alt="" /></div>
        </div>
	</footer>
	
	<div class="header-rsp">
		<div class="logo-rsp"><a href="/"></a></div>
		<button class="hamburger hamburger--collapse" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
	</div>

	<div class="menu-rsp">
		<div class="opacity-rsp"></div>
		<nav></nav>
	</div>

	<script type="text/javascript" src="<? echo base_url()?>cookies/divante.cookies.min.js"></script>
	<script>window.jQuery.cookie || document.write('<script src="<? echo base_url()?>cookies/jquery.cookie.min.js"><\/script>')</script>
	<script type="text/javascript">
		jQuery.divanteCookies.render({
		    privacyPolicy : true,
		    cookiesPageURL : ''
		});
	</script>
</body>
</html>