<div class="main-content">
        <div class="container">
            
            <div class="title">ZASADY</div>
            
            <div class="content-box content-box-1">
                
                <div class="boxes-1">
                    <div class="row-box">
                        <div class="col-box">
                            <div class="box-1">
                                <div class="number">1</div>
                                <div class="txt">
                                    Stwórzcie z kolegami z&nbsp;oddziału 3-osobową drużynę <br />(kapitan + dwóch zawodników).
                                </div>
                            </div>
                        </div>
                        <div class="col-box">
                            <div class="box-1">
                                <div class="number">2</div>
                                <div class="txt">
                                    Sprzedawajcie źródła światła LED, oprawy Ledinaire, PILA i Malaga LED i weźcie udział w&nbsp;zespołowej rywalizacji.
                                </div>
                            </div>
                        </div>
                        <div class="col-box">
                            <div class="box-1">
                                <div class="number">3</div>
                                <div class="txt">
                                    Wygrywajcie <strong>pojedynki sprzedażowe </strong> z&nbsp;przeciwnikami w swojej lidze! 
                                    Nagrodzimy Was za&nbsp;każdy zwycięski pojedynek sprzedażowy!
                                </div>
                                <div class="button-box">
                                    <a href="<? echo base_url()?>zasady/pojedynki" class="btn-2">POZNAJ SZCZEGÓŁY</a>
                                </div>                                
                            </div>
                        </div>
                        <div class="col-box">
                            <div class="box-1">
                                <div class="number">4</div>
                                <div class="txt">
                                    Zbierajcie ekstra punkty za&nbsp;zrealizowanie <strong>misji specjalnych.</strong>
                                </div>
                                <div class="button-box">
                                    <a href="<? echo base_url()?>zasady/misje-specjalne" class="btn-2">POZNAJ SZCZEGÓŁY</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-box">
                            <div class="box-1">
                                <div class="number">5</div>
                                <div class="txt">
                                    Zawalczcie o <strong>zwycięstwo w rankingu i nagrodę główną!</strong>
                                </div>
                                <div class="button-box">
                                    <a href="<? echo base_url()?>nagrody" class="btn-2">POZNAJ SZCZEGÓŁY</a>
                                </div>                                
                            </div>
                        </div>                        
                    </div>
                </div>

                <div class="button-box-1">
                    <a href="<? echo base_url()?>files/regulamin.pdf" target="_blank" class="btn-3">ZOBACZ REGULAMIN</a>
                </div>
                
            </div>
            
        </div>
    </div>