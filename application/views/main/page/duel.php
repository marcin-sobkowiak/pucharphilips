  <div class="main-content">
        <div class="container">
            
            <div class="title margin-bottom-1">
                POJEDYNKI SPRZEDAŻOWE
                <div class="title-button">
                    <a href="<? echo base_url()?>zasady" class="btn-4">WRÓĆ</a>
                </div>
            </div>
            
            <div class="content-box content-box-2">
               
                <div class="col-box-1">
                    <div class="txt-1">
                        Co miesiąc rozgrywacie jeden pojedynek sprzedażowy z drużyną przeciwną ze swojej ligi (regionu).
                    </div>
                    <div class="txt-1">
                        Zwycięzcą pojedynku sprzedażowego jest drużyna, która w danym miesiącu osiągnie  większy obrót w sprzedaży produktów źródeł światła LED, oprawy Ledinaire, PILA i Malaga LED.
                    </div>
                    <div class="txt-1">
                        Za każdy zwycięski pojedynek Twoja drużyna otrzyma punkty w&nbsp;rankingu oraz dodatkową nagrodę w postaci premii pieniężnej na kartę prepaid.
                    </div>
                    <div class="txt-1">
                        Im więcej wygranych pojedynków, tym więcej punktów w rankingu i wyższa premia dla drużyny! 
                    </div>
                    <div class="txt-1">
                        <strong>Warunkiem otrzymania nagrody jest uzyskanie miesięcznej sprzedaży powyżej 5000 zł netto.</strong>
                    </div> 
                </div>
                
                <div class="col-box-2">
                    <div class="txt-2">
                        <div class="txt-2-1">Premia 50 zł</div>
                        za zwycięstwo w I i II pojedynku
                    </div>
                    <div class="txt-2">
                        <div class="txt-2-1">Premia 100 zł</div>
                        za zwycięstwo w III i IV pojedynku
                    </div>
                    <div class="txt-2">
                        <div class="txt-2-1">Premia 250 zł</div>
                        zł za zwycięstwo w V i VI pojedynku
                    </div>                    
                </div>
               
            </div>
            
        </div>
    </div>