<div class="main-content">
        <div class="container">

            <div class="content-box x-content-box-1 x-content-box-1-js-1">
                <div class="content-box-4">
                    <div class="nav-tabs-1">
                        <ul>
                            <li class="current"><a href="" class="1">Nagroda główna</a></li>
                            <li><a href="" class="2">Nagroda pojedynki sprzedażowe</a></li>
                        </ul>
                    </div>                    
                    <p>
                        Poprowadź swoją drużynę na szczyt<br />rankingu, aby wygrać
                    </p>
                    <h2>UDZIAŁ<br /> W NIEZAPOMNIANYM WYDARZENIU!</h2>
                    <p>
                        W tegorocznej edycji Pucharu Philips wygrywa 
			7&nbsp;drużyn. Po jednej drużynie z każdej ligi otrzyma zaproszenie do wysokiej klasy ośrodka w Polsce 
			na specjalne spotkanie, na którym będzie na nich czekało wiele niespodzianek i atrakcji.
                    </p>
                    <strong>Szczegóły wkrótce.</strong>
                    <figure><img src="<? echo base_url()?>img/files/img02.png" alt="" /></figure>
                </div>
                <div class="x-image">
                    <img src="<? echo base_url()?>img/files/img01.png" alt="" />
                </div>  
                
            </div>
          
            <div class="content-box x-content-box-1 x-content-box-1-js-2" style="display: none;">
                <div class="content-box-5">
                    <div class="nav-tabs-1">
                        <ul>
                            <li><a href="" class="1">Nagroda główna</a></li>
                            <li class="current"><a href="" class="2">Nagroda pojedynki sprzedażowe</a></li>
                        </ul>
                    </div>
                    <div class="description">
                        <div class="txt-1">
                            Zdobywaj z drużyną<br />
                            <strong>doładowania karty prepaid premią <br />
                            pieniężną za każdy wygrany mecz!</strong>
                        </div>
                        <div class="txt-2">
                            <div class="txt-2-1">Premia 50 zł</div>
                            za zwycięstwo w I i II pojedynku
                        </div>
                        <div class="txt-2">
                            <div class="txt-2-1">Premia 100 zł</div>
                            za zwycięstwo w III i IV pojedynku
                        </div>
                        <div class="txt-2 txt-2-last">
                            <div class="txt-2-1">Premia 250 zł</div>
                            za zwycięstwo w V i VI pojedynku
                        </div>
                    </div>
                    <figure><img src="<? echo base_url()?>img/files/img04.png" alt="" /></figure>
                </div>
                <div class="x-image">
                    <img src="<? echo base_url()?>img/files/img03.png" alt="" />
                </div>  
                
            </div>            
            
        </div>
    </div>