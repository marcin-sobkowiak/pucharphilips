  <div class="main-content">
        <div class="container">
            
            <div class="title margin-bottom-1">
                RYWALIZACJA W RANKINGU i MISJE SPECJALNE
                <div class="title-button">
                    <a href="<? echo base_url()?>zasady" class="btn-4">WRÓĆ</a>
                </div>
            </div>
            
            <div class="content-box-3">
               <p>Przez cały czas trwania rozgrywek pucharowych <strong>WASZA DRUŻYNA ZBIERA PUNKTY,</strong> które decydują o Waszej <strong>POZYCJI W RANKINGU W DANEJ LIDZE (REGIONIE).</strong></p>
               <p>Punkty można zyskać wygrywając pojedynki sprzedażowe <br />oraz realizując <strong>MISJE SPECJALNE. </strong></p>
                <p><strong>Za najlepszą realizację Misji Specjalnych  3  pierwsze Drużyny <br />otrzymują nagrodę dodatkową!</strong></p>
                <p>Treść misji specjalnej oraz nagroda za ich realizację <br />będzie ogłaszana na początku każdego miesiąca rozgrywek.<br />
			Każda zrealizowana misja specjalna <strong>zwiększa ilość <br />zdobytych punktów w rankingu o dodatkowe 5%! </strong></p>
               
                <div class="button-box">
                <a href="" class="btn-3 btn-3-1 btn-open-1"><span>ZOBACZ PRZYKŁAD</span><span style="display: none;">ZWIŃ</span></a>
               </div>
               
               <script>
                $(function(){
                   $('.btn-open-1').click(function(){
                        $('.aa-content-box').fadeToggle();
                        $(this).find('span').toggle();
                        return false;
                   });
                });
               </script>
               
               <div class="aa-content-box" style="display: none;">
                <table>
                    <tr>
                        <th>Liczba punktów<br /> drużyny w rankingu</th>
                        <th>Liczba zrealizowanych<br /> misji zakupowych</th>
                        <th>Uzyskany mnożnik punktowy</th>
                        <th>Całkowita liczba punktów w rankingu po realizacji misji zakupowych</th>
                    </tr>
                    <tr>
                        <td><strong>12</strong></td>
                        <td><strong>4</strong></td>
                        <td><strong>+20%</strong></td>
                        <td><strong>14</strong> [bo: 20% z 12 punktów = 2,4 a: 12 punktów + 2,4 = 14,4&asymp;14]</td>
                    </tr>
                </table>
               </div>
               <p>W tegorocznej edycji Pucharu Philips wygrywa 7 drużyn. <br />Po jednej drużynie z każdej ligi (regionu), która zdobędzie najwięcej punktów.</p>
               
            </div>
            
        </div>
    </div>