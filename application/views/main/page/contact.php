<div class="main-content">
        <div class="container">

            <div class="content-box-9">
               <div class="title title-1">Masz pytania?<br />Skontaktuj się z nami.</div>
            
                <div class="form-box">
                    <form action="#" method="post">
                        <div class="label-box">
                            <label>IMIĘ I NAZWISKO</label>
                        </div>
                        <div class="input-box">
                            <input type="text"  name="name"  required  value="<? echo set_value('name')?>" />
                            <input type="hidden"  name="trigger"  required  value="<? echo sha1($this->session->session_id)?>" />
                        </div>
                        <div class="label-box">
                            <label>ADRES E-MAIL</label>
                        </div>
                        <div class="input-box">
                            <input type="email" name="email"  required value="<? echo set_value('email')?>" />
                        </div>
                        <div class="label-box">
                            <label>WIADOMOŚĆ</label>
                        </div>
                        <div class="input-box">
                            <textarea name="content" required><? echo set_value('content')?></textarea>
                        </div>
                        
                        <div class="f-button-box">
                            <button type="submit" class="btn-3 width-1">WYŚLIJ WIADOMOŚĆ</button>
                        </div>
                        
                    </form>
                </div>
                
                <figure><img src="<? echo base_url()?>img/files/img09.png" alt="" /></figure>
            
            </div>

        </div>
    </div>