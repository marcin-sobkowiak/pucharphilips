<div class="main-content">
	<div class="container">
		<div class="content-box x-content-box-1">
			<div class="content-box-11">                   
                                      <div class="title title-1">Witaj w IV edycji <br />Pucharu Philips</div>
                                      <div class="txt-1">Włącz się do rywalizacji i walcz o zwycięstwo!</div>
				<?php if(form_error('login')) {?><div class="alert alert-danger"><? echo form_error('login')?></div><?}?>
				<?php if(form_error('password')) {?><div class="alert alert-danger"><? echo form_error('password')?></div><?}?>
				<? if($error==1) {?><div class="alert alert-danger">Błędne dane logowania</div><?}?>
				<div class="form-box-2">
					<form action="" method="post">
						    <div class="label-box">
							<label>ADRES E-MAIL</label>
						    </div>
						    <div class="input-box">
							<input type="email" name="login"  required />
						    </div>
						    <div class="label-box">
							<label>HASŁO</label>
						    </div>
						    <div class="input-box">
							<input type="password" name="password"  required  />
						    </div>
						    <div class="txt-2">
							<a href="<? echo base_url()?>przypominanie-hasla">Nie pamiętasz hasła?</a>
						    </div>
						    <div class="z-button-box-1">
							<button type="submit" class="btn-3">ZALOGUJ SIĘ</button>
						    </div>
					</form>
						    <div class="txt-3">
							<strong>Nie masz jeszcze konta?</strong>
							Zbierz drużynę, zostań kapitanem<br /> i wejdź do gry!
						    </div>
						    <div class="z-button-box-2">
							<a href="<? echo base_url()?>rejestracja" class="btn-3 btn-3-red">ZGŁOŚ SWOJĄ DRUŻYNĘ</a>
						    </div>                            
					
				</div>
			<figure><img src="<? echo base_url()?>img/files/img11.png" alt="" /></figure>
			</div>
			<div class="x-image"><img src="<? echo base_url()?>img/files/img10.png" alt="" /></div>  
		</div>
	</div>
    </div>