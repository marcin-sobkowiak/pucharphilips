<div class="main-content">
        <div class="container">

            <div class="content-box-10">
               <div class="title">ZGŁOŚ SWOJĄ DRUŻYNĘ</div>
            
                <div class="form-box-1">
                    <form action="#" method="post">
                        
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="f-title">KAPITAN</div>
                                <div class="label-box">
                                    <label>IMIĘ</label>
                                </div>
                                <div class="input-box">
				<input type="text"  required  name="name1" value="<? echo set_value('name1')?>"  />
                                </div>
                                <div class="label-box">
                                    <label>NAZWISKO</label>
                                </div>
                                <div class="input-box">
                                    <input type="text"  required  name="surname1" value="<? echo set_value('surname1')?>"  />
                                </div>
                                <div class="label-box">
                                    <label>TELEFON KONTAKTOWY</label>
                                </div>
                                <div class="input-box">
                                    <input type="text"  required  name="phone" value="<? echo set_value('phone')?>" class="phone"  />
                                </div>
                                <div class="label-box">
                                    <label>ADRES E-MAIL</label>
                                </div>
                                <div class="input-box">
                                      <input type="email"  required  name="email" value="<? echo set_value('email')?>"  />
                                </div>                                
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <div class="f-title">DANE ODDZIAŁU</div>
                                <div class="label-box">
                                    <label>NAZWA DYSTRYBUTORA</label>
                                </div>
                                <div class="input-box">
                                       <input type="text"  required  name="firm" value="<? echo set_value('firm')?>"  />
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="label-box">
                                            <label>NIP DYSTRYBUTORA</label>
                                        </div>
                                        <div class="input-box">
                                            <input type="text"  required  name="nip" value="<? echo set_value('nip')?>"  class="nip" />
                                        </div>  
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="label-box">
                                            <label>ULICA ODDZIAŁU</label>
                                        </div>
                                        <div class="input-box">
                                             <input type="text"  required  name="street" value="<? echo set_value('street')?>"  />
                                        </div>  
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="label-box">
                                            <label>NR DOMU ODDZIAŁU</label>
                                        </div>
                                        <div class="input-box">
                                              <input type="text"  required  name="home" value="<? echo set_value('home')?>"  />
                                        </div>  
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="label-box">
                                            <label>NR LOKALU ODDZIAŁU</label>
                                        </div>
                                        <div class="input-box">
                                           <input type="text"    name="number" value="<? echo set_value('number')?>"  />
                                        </div>  
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="label-box">
                                            <label>KOD POCZTOWY ODDZIAŁU</label>
                                        </div>
                                        <div class="input-box">
                                            <input type="text"  required  name="zip" value="<? echo set_value('zip')?>"  class="zip" />
                                        </div>  
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="label-box">
                                            <label>MIEJSCOWOŚĆ ODDZIAŁU</label>
                                        </div>
                                        <div class="input-box">
                                             <input type="text"  required  name="city" value="<? echo set_value('city')?>"  />
                                        </div>  
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                        
                        <div class="f-title">DRUŻYNA</div>
                        
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="label-box">
                                    <label>NAZWA ZESPOŁU</label>
                                </div>
                                <div class="input-box">
                                      <input type="text"  required  name="login" value="<? echo set_value('login')?>"  />
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="label-box">
                                    <label>IMIĘ 1. ZAWODNIKA</label>
                                </div>
                                <div class="input-box">
                                  <input type="text"  required  name="name2" value="<? echo set_value('name2')?>"  />
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="label-box">
                                    <label>NAZWISKO 1. ZAWODNIKA</label>
                                </div>
                                <div class="input-box">
                                     <input type="text"  required  name="surname2" value="<? echo set_value('surname2')?>"  />
                                </div>
                            </div>                            
                        </div>                        
                        
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="label-box">
                                    <label>IMIĘ 2. ZAWODNIKA</label>
                                </div>
                                <div class="input-box">
                                    <input type="text"  required  name="name3" value="<? echo set_value('name3')?>"  />
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="label-box">
                                    <label>NAZWISKO 2. ZAWODNIKA</label>
                                </div>
                                <div class="input-box">
                                     <input type="text"  required  name="surname3" value="<? echo set_value('surname3')?>"  />
                                </div>
                            </div>                            
                        </div>
                        
                        <div class="checkbox-content">
                            <div class="checkbox-box">
                                <input type="checkbox"  name="agree1" value="1" required id="agree1" <?if(set_value('agree1')==1){?>checked<?}?> />
                                <label for="agree1">Oświadczam, że zgłaszam udział Drużyny ze wskazanej hurtowni w Konkursie „Puchar Philips” przy wykorzystaniu podanych wyżej danych, a także, że cała Drużyna zapoznała się i akceptuje <a href="<? echo base_url()?>files/regulamin.pdf" target="_blank"> regulamin</a> tego Konkursu. </label>
                            </div>
                            <div class="checkbox-box">
                                <input type="checkbox"  name="agree2" value="1" required id="agree2" <?if(set_value('agree2')==1){?>checked<?}?> />
                                <label for="agree2">Oświadczam, że uzyskałem/uzyskałam zgodę od pracodawcy (właściciela hurtowni) na swój udział oraz udział pozostałych członków drużyny w Konkursie „Puchar Philips”. </label>
                            </div>
                            <div class="checkbox-box">
                                <input type="checkbox"  name="agree3" value="1"  id="agree3" <?if(set_value('agree3')==1){?>checked<?}?> />
                                <label for="agree3">Chcę otrzymywać spersonalizowane wiadomości o charakterze promocyjnym na temat produktów, usług, wydarzeń i promocji, dotyczące marek powiązanych z Signify. Dowiedz się więcej </label>
                            </div>
                            <div class="checkbox-box">
                                <input type="checkbox"  name="agree4" value="1" required id="agree4" <?if(set_value('agree4')==1){?>checked<?}?> />
                                <label for="agree4">Akceptuję niniejsze Zasady korzystania  <br />Wszystkie dane osobowe będą przetwarzane zgodnie z niniejszymi <a href="<? echo base_url()?>files/polityka_prywatnosci.pdf" target="_blank">Polityka prywatności </a> * </label>
                            </div>
                            <div class="legend-box">
                                *pola wymagane
                            </div>
                        </div>
                     
                        <div class="f-button-box">
                            <button type="submit" class="btn-3 width-1">ZAREJESTRUJ</button>
                        </div>
                        
                    </form>
                </div>

            </div>

        </div>
    </div>
	<script type="text/javascript" src="<? echo base_url()?>scripts/js/jquery.maskedinput.js"></script>    
<script>
$(document).ready(function() {    
	$(".phone").mask("999999999");
	$(".zip").mask("99-999");
	$(".nip").mask("9999999999");


});
</script>  	