 <div class="main-content">
        <div class="container">
            <div class="content-box x-content-box-1">
                <div class="content-box-7">                   
                    <div class="txt-1">
                        Wygrywajcie w pojedynkach sprzedażowych z&nbsp;drużynami przeciwnymi w swojej lidze.<br />Im&nbsp;więcej zwycięskich meczów, tym więcej punktów i&nbsp;większa premia dla Twojego zespołu.
                    </div>
                    <div class="txt-2">
                        HARMONOGRAM ROZGRYWEK <br />I WYNIKI POJEDYNKÓW
                    </div>
                    <div class="slider slider-1">
                        <ul>
			<? foreach($duels as $item):?>
                            <li>
                                <div class="s-nav s-nav-1">
                                    <div class="prev-box"></div>
                                    <div class="month"><? echo $item['month']?>   </div>
                                    <div class="next-box"></div>
                                </div>
                                
                                <div class="txt-3">
                                    <div class="col-box col-box-1">
                                        <div class="image"><img src="<? echo base_url()?>img/icons/img04.png" alt="" /></div>
                                        <? echo $this->_user['login'] ?>
                                    </div>
                                    <div class="col-box col-box-2">VS</div>
                                    <div class="col-box col-box-1 col-box-1-gray">
                                        <div class="image"><img src="<? echo base_url()?>img/icons/img05.png" alt="" /></div>
                                      <? echo $item['opponent']?>   
                                    </div>
                                </div>
                                <div class="txt-4">
                                    STATUS:<br /><span><? echo $item['status']?></span>
                                </div>
                                <div class="txt-5">
                                    WYNIK<br />
                                    <span><? echo $item['points']?> : <? echo $item['points2']?></span>
                                </div>
                                <div class="txt-6">
                                    <div class="txt-6-1">
                                        <span class="color-green">WYGRANE</span> / <span class="color-red">PRZEGRANE</span>
                                    </div>
                                    <div class="txt-6-2">
                                        <span class="color-green"><? echo $result['win']?></span> / <span class="color-red"><? echo $result['loss']?></span>
                                    </div>                                    
                                </div>
                            </li>
                              <? endforeach?>                         
                        </ul>
                    </div>
                    <figure><img src="<? echo base_url()?>img/files/img07.png" alt="" /></figure>
                </div>
                <div class="x-image">
                    <img src="<? echo base_url()?>img/files/img06.png" alt="" />
                </div>  
            </div>
        </div>
    </div>