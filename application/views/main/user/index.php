	<link rel="stylesheet" href="<? echo base_url()?>css/jquery.jscrollpane.css" type="text/css" />	
 	<script src="<? echo base_url()?>scripts/js/jquery.jscrollpane.min.js"></script>
	<script src="<? echo base_url()?>scripts/js/jquery.mousewheel.js"></script>	
	<script>
		$(function() {
            if ($('body').width() > 992) {
                $('.scroll-pane').jScrollPane({
                    verticalDragMinHeight: 5,
                    verticalDragMaxHeight: 65
                });
            }
        });
	</script> 	    
	
  <div class="main-content">
        <div class="container">

            <div class="content-box x-content-box-1">
                <div class="content-box-6">                   
                    <div class="description">
                        <h2>RANKING DRUŻYNOWY ROZGRYWEK O PUCHAR PHILIPS</h2>
                        <p>Sprawdzaj aktualną pozycję swojej drużyny w rozgrywkach o Puchar Philips i poprowadź ją do zwycięstwa! Pnijcie się&nbsp;na sam szczyt rankingu!</p>
                    </div>
                    
                    <div class="scroll-pane">
                    
                        <div class="table-box">
                              <table>
                                <tr>
                                    <th><div <?/*class="arrow-1"*/?>>POZYCJA</div></th>
                                    <th><div <?/*class="arrow-1"*/?>>DRUŻYNA</div></th>
                                  <?/*  <th><div class="arrow-1">REGION/LIGA</div></th>*/?>
                                    <th><div <?/*class="arrow-1"*/?>>LICZBA PUNKTÓW</div></th>
                                </tr>
			<? $i=1; foreach($rank as $item):?>
			<tr <? if($item['idUser']==$this->_user['idUser']) {?>class="current"<?}?>>
                                    <td><? echo $i?></td>
                                    <td><? echo $item['login']?></td>
                                     <?/*    <td>Wielkopolska</td>*/?>
                                    <td><? echo round($item['points'])?></td>
                                </tr>
			<? $i++; endforeach ?>	
				
                               
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="x-image">
                    <img src="<? echo base_url()?>img/files/img05.png" alt="" />
                </div>  
                
            </div>

        </div>
    </div>	