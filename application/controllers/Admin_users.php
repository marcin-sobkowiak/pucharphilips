<?php
class Admin_users extends MY_Controller {
function __construct() {
	parent::__construct();
	if(!$this->_admin['logged']) redirect('admin');
}	
function index()
{
	$cond=array();
	$page= $this->user_model->getList('login,email,id,phone,active,registerDate,firm,nip',$cond);
	$tablica=array('pageFile'=>'users/index.php','page'=>$page);
	$this->_displayAdmin('admin/template',$tablica);
	
}
function newUsers()
{
	$cond=array('idRank'=>NULL);
	$page= $this->user_model->getList('login,email,id,phone,active,registerDate,firm,nip',$cond);
	$tablica=array('pageFile'=>'users/newUsers.php','page'=>$page);
	$this->_displayAdmin('admin/template',$tablica);
	
}
function _rank($type=1)
{
	$cond=array();
	$rank = $this->user_model->getRank('idUser,points,name,surname,lang',array('level'=>$type));/* ranking */
	$tablica=array('pageFile'=>'users/rank.php','page'=>$rank);
	$this->_displayAdmin('admin/template',$tablica);
	
}
function duels($month='')
{
$month=$this->security->xss_clean($month);
	$duels=array();
	/*wrzesien*/
	if($month) {
		$pair = $this->user_model->getGameListSuate('*',array('month'=>$month));
	} else {
		$pair = $this->user_model->getGameListSuate('*',array());
	}

	foreach($pair as $item) {
		$game=array('winner'=>'');	
		$player1= $this->user_model->getSingle('login',array('id'=>$item['idPlayer1']));
		$points1= $this->user_model->getSinglePoints('points',array('idUser'=>$item['idPlayer1'],'month'=>$month));
		if(empty($points1)) $points1['points']=0;
		
		$player2= $this->user_model->getSingle('login',array('id'=>$item['idPlayer2']));
		$points2= $this->user_model->getSinglePoints('points',array('idUser'=>$item['idPlayer2'],'month'=>$month));
		if(empty($points2)) $points2['points']=0;
		
		if($item['winner']==$item['idPlayer1']) $game['winner']= $player1['login'];
		if($item['winner']==$item['idPlayer2']) $game['winner']= $player2['login'];
		$game['id'] = $item['id'];
		$game['player1'] = $player1['login'];
		$game['points1'] = $points1['points'];
		$game['idPlayer1'] =$item['idPlayer1'];
		
		$game['player2'] = $player2['login'];
		$game['points2'] = $points2['points'];
		$game['idPlayer2'] =$item['idPlayer2'];

		array_push($duels,$game);
	}
	$tablica=array('pageFile'=>'users/duels.php','duels'=>$duels,'month'=>$month);
	$this->_displayAdmin('admin/template',$tablica);
}
function edit($id)
{
	$id=(int)$id;
	$cond=array('id'=>$id);
	$user = $this->user_model->getSingle('*',$cond);
	if(empty($user)) redirect('admin_users');

//	$rank = $this->user_model->getRankPH('idUser,points,name,surname,lang',array('level'=>$user['level']));/* ranking */

	
//	$result=$this->user4->userRank($rank,$user['id']) ;
	
//	$positionFinal = $this->user4->userFinalRank($rankFinal,$user['id']);
	
	$config=array(
			array('field'   => 'email','label'   => 'E-mail','rules'   => 'trim|xss_clean|valid_email|checkEmail['.$user['id'].']'),
			array('field'   => 'name1','label'   =>'Imię Kapitana','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'surname1','label'   =>'Nazwisko Kapitana','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'name2','label'   =>'Imię 1 zawodnika','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'surname2','label'   =>'Nazwisko 1 zawodnika','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'name2','label'   =>'Imię 2 zawodnika','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'surname2','label'   =>'Nazwisko 2 zawodnika','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'phone','label'   => 'Telefon','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'firm','label'   => 'Nazwa dystrybutora','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'street','label'   => 'Ulica oddziału','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'home','label'   => 'Nr domu oddziału','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'number','label'   => 'Nr lokalu oddziału','rules'   => 'trim|xss_clean'),
			array('field'   => 'zip','label'   => 'Kod pocztowy oddziału','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'city','label'   => 'Miejscowość oddziału','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'nip','label'   => 'NIP','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'login','label'   => 'Nazwa zespołu','rules'   => 'trim|required|xss_clean|checkLogin['.$user['id'].']'),
			array('field'   => 'idRank','label'   => 'Region rankingowy','rules'   => 'trim|xss_clean'),
			);

	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{
		$tablica=array('pageFile'=>'users/edit.php','page'=>$user);
		$this->_displayAdmin('admin/template',$tablica);	
	} else {
		$active=0;
		if($this->input->post('active')) $active=1;
		$insert = array(
					'name1'=>$this->input->post('name1'),
					'surname1'=>$this->input->post('surname1'),
					'name2'=>$this->input->post('name2'),
					'surname2'=>$this->input->post('surname2'),
					'name3'=>$this->input->post('name3'),
					'surname3'=>$this->input->post('surname3'),
					'email'=>$this->input->post('email'),
					'phone'=>$this->input->post('phone'),
					'login'=>$this->input->post('login'),
					'firm'=>$this->input->post('firm'),
					'street'=>$this->input->post('street'),
					'number'=>$this->input->post('number'),
					'home'=>$this->input->post('home'),
					'zip'=>$this->input->post('zip'),
					'nip'=>$this->input->post('nip'),
					'city'=>$this->input->post('city'),
					'idRank'=>$this->input->post('idRank'),
					'active'=>$active,
					'editAdmin'=>$this->_admin['idAdmin']
					);
		

		$this->db->update('users',$insert,array('id'=> $id));		
		$this->session->set_flashdata('update','Dane zostały zaaktualizowane');
		redirect('admin_users/edit/'.$id);
	}	
}

}// koniec
?>
