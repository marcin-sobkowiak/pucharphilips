<?php
class Admin_utilities extends MY_Controller {
function __construct() {
	parent::__construct();
	if(!$this->_admin['logged']) redirect('admin');
}

function index()
{
	$users= count($this->user_model->getList('id',array('idRank'=>NULL)));
	$tablica=array('title'=>'Start','pageFile'=>'utilities/start.php','users'=>$users);
	$this->_displayAdmin('admin/template',$tablica);	 
}
function password()
{
  $tablica=array('title'=>'Zmiana hasła','pageFile'=>'utilities/password.php');
      $config = array(
               array('field'   => 'stare','label'   => 'stare','rules'   => 'trim|required|xss_clean'),
               array('field'   => 'nowe1','label'   => 'nowe1','rules'   => 'trim|required|xss_clean|min_length[8]'),
               array('field'   => 'nowe2','label'   => 'nowe2','rules'   => 'trim|required|xss_clean|matches[nowe1]')
            );
   $this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{
	$this->_displayAdmin('admin/template',$tablica);	
	} else {
        $logData = $this->admin_model->getSingle('*',array('idAdmin'=>$this->_admin['idAdmin'],'pass'=>sha1($this->input->post('stare'))));
		if ($logData) {
			$newDate =  strtotime(date('Y-m-d',time()));
			$newDate+= 24 * 60 * 60 * 30;
			$newDate = date("Y-m-d",$newDate);
			
		$this->db->update('admins',array('pass'=>sha1($this->input->post('nowe1')),'passwordDate'=>$newDate),array('idAdmin'=>$this->_admin['idAdmin']));
		$this->session->set_flashdata('update', 'hasło zostało zmienione');
		}  else {
		$this->session->set_flashdata('error', 'Podaj poprawne stare hasło'); 
		} 
       redirect('admin_utilities/password');
      } 
}
 function _admin()
{	
if($this->_admin['adminType']!='god') {
   redirect('admin_utilities');
}
	$admins =$this->admin_model->getList('*',array());
	$data = array('title'=>'Administracja','pageFile'=>'utilities/admins.php','admins'=>$admins);
	$this->_displayAdmin('admin/template',$data);
}
function _edit($id)
{	
if($this->_admin['adminType']!='god') {
   redirect('admin_utilities');
}
	$id=(int)$id;
	$page=$this->admin_model->getSingle('*',array('idAdmin'=>$id));
	if(empty($page)) {
		redirect('admin_utilities/admin');
	}
	$admin =     $this->admin_model->getSingle('*',array('idAdmin'=>$page['idAdmin'])); /* dane admina */
	$config = array(
               array('field'   => 'active','label'   => 'active','rules'   => 'xss_clean'),
               array('field'   => 'name','label'   => 'name','rules'   => 'xss_clean|required'),
               array('field'   => 'login','label'   => 'login','rules'   => 'xss_clean|required|min_length[3]'),
               array('field'   => 'password','label'   => 'password','rules'   => 'xss_clean|min_length[6]'),
            );
	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{	
		$data = array('pageFile'=>'utilities/editAdmin.php','page'=>$page,'title'=>'Edycja Admina','admin'=>$admin);
		$this->_displayAdmin('admin/template',$data);
	} else {
		$insert = array('name'=>$this->input->post('name'),
				'login'=>$this->input->post('login'),
				'active'=>$this->input->post('active'),
				'editDate'=>$this->_actDate,
				);
		if($this->input->post('password')!='') {
			$insert['pass']=	sha1($this->input->post('password'));
		}
		$this->db->update('admins',$insert,array('idAdmin'=>$id));	
	$this->session->set_flashdata('update','Admin został edytowany');			
	redirect('admin_utilities/edit/'.$id);		
	}
}
function _add()
{
if($this->_admin['adminType']!='god') {
   redirect('admin_utilities');
}
	$config = array(
               array('field'   => 'active','label'   => 'active','rules'   => 'xss_clean'),
               array('field'   => 'name','label'   => 'name','rules'   => 'xss_clean|required'),
               array('field'   => 'login','label'   => 'login','rules'   => 'xss_clean|required|min_length[3]'),
               array('field'   => 'password','label'   => 'password','rules'   => 'xss_clean|required|min_length[6]'),
            );
	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{	
		$data = array('pageFile'=>'utilities/addAdmin.php','title'=>'Dodawanie Admina');
		$this->_displayAdmin('admin/template',$data);
	} else {
		$insert = array('name'=>$this->input->post('name'),
				'login'=>$this->input->post('login'),
				'active'=>$this->input->post('active'),
				'pass'=>sha1($this->input->post('password')),
				'editDate'=>$this->_actDate,
				'addDate'=>$this->_actDate,
				);
		$this->db->insert('admins',$insert);	
		$page=$this->admin_model->getSingle('max(idAdmin) as id',array());
	$this->session->set_flashdata('update','Admin został dodany');			
	redirect('admin_utilities/edit/'.$page['id']);		
	}
}


}// koniec
?>
