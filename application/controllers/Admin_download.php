<?php
class Admin_download extends MY_Controller {
function __construct() {
	parent::__construct();
		
	$this->load->helper('file');
	$this->load->helper('download');
	$this->load->library('excel');		
	if(!$this->_admin['logged']) redirect('admin');
	//$this->_ph = array(0=>'',1=>'Aleksandrowicz Krzysztof',2=>'Frychel Jarosław',3=>'Leśniak Marcin',4=>'Pawłowski Mirosław',5=>'Pietrusiak Andrzej',6=>'Sadlej Paweł',7=>'Zabłocki Jacek');
	$this->_styleArray1 = array(
						'font' => array('bold' => true),
						'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						);
	$this->_styleArray2 = array(
						'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						);
	$this->_styleArray3 = array(
						'font' => array('bold' => true),
						);
	$this->_styleArray4 = array(
						'font' => array('bold' => true),
						'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
										'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
										'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
										'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
									),
						);
	$this->_styleArray5 = array(
						'format'=>array('code' =>PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1),
						);
						
	$this->_letters = array(1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z',27=>'AA',28=>'AB',29=>'AC',30=>'AD',
						31=>'AE',32=>'AF',33=>'AG',34=>'AH',35=>'AI',36=>'AJ',37=>'AK',38=>'AL',39=>'AM',40=>'AN',41=>'AO',42=>'AP',43=>'AQ',44=>'AR',45=>'AS',46=>'AT',47=>'AU',48=>'AV',49=>'AW',50=>'AX',51=>'AY',52=>'AZ',53=>'BA',54=>'BB',55=>'BC',
						56=>'BD',57=>'BE',58=>'BF',59=>'BG',60=>'BH',61=>'BI',62=>'BJ',63=>'BK',64=>'BL',65=>'BM',66=>'BN',67=>'BO',68=>'BP',69=>'BQ',70=>'BR',71=>'BS',72=>'BT',73=>'BU',74=>'BV',75=>'BW',76=>'BX',77=>'BY',78=>'BZ',79=>'CA',80=>'CB',
						81=>'CC',82=>'CD',83=>'CE',84=>'CF',85=>'CG',86=>'CH',87=>'CI',88=>'CJ',89=>'CK',90=>'CL',91=>'CM',92=>'CN',93=>'CO',94=>'CP',95=>'CQ',96=>'CR',97=>'CS',98=>'CT',99=>'CU',100=>'CV',101=>'CW',102=>'CX',103=>'CY',104=>'CZ',
						105=>'DA',106=>'DB',107=>'DC',108=>'DD',109=>'DE',110=>'DF',111=>'DG',112=>'DH',113=>'DI',114=>'DJ',115=>'DK',116=>'DL',117=>'DM',118=>'DN',119=>'DO',120=>'DP',121=>'DQ',122=>'DR',123=>'DS',124=>'DT',125=>'DU',126=>'DV',
						127=>'DW',128=>'DX',129=>'DY',130=>'DZ',131=>'EA',132=>'EB',133=>'EC',134=>'ED',135=>'EE',136=>'EF',137=>'EG',138=>'EH',139=>'EI',140=>'EJ',141=>'EK',142=>'EL',143=>'EM',144=>'EN',145=>'EO',146=>'EP',147=>'EQ',148=>'ER',
						149=>'ES',150=>'ET',151=>'EU',152=>'EV',153=>'EW',154=>'EX',155=>'EY',156=>'EZ',157=>'FA',158=>'FB',159=>'FC',160=>'FD',161=>'FE',162=>'FF',163=>'FG',164=>'FH',165=>'FI',166=>'FJ',167=>'FK',168=>'FL',169=>'FM',170=>'FN',
						171=>'FO',172=>'FP',173=>'FQ',174=>'FR',175=>'FS',176=>'FT',177=>'FU',178=>'FV',179=>'FW',180=>'FX',181=>'FY',182=>'FZ',183=>'GA',184=>'GB',185=>'GC',186=>'GD',187=>'GE',188=>'GF');					
}

function users() {
$this->excel->getActiveSheet()->setTitle('Arkusz 1');
$this->excel->getActiveSheet()->mergeCells('A1:L1');
$this->excel->getActiveSheet()->setCellValue('A1', 'DANE DRUŻYNY')->getStyle('A1')->applyFromArray($this->_styleArray1);
$this->excel->getActiveSheet()->mergeCells('M1:S1');
$this->excel->getActiveSheet()->setCellValue('M1', 'DANE DYTRYBUTORA')->getStyle('M1')->applyFromArray($this->_styleArray1);
$this->excel->getActiveSheet()->mergeCells('T1:W1');
$this->excel->getActiveSheet()->setCellValue('T1', 'ZGODY')->getStyle('T1')->applyFromArray($this->_styleArray1);

$a=1;
$firstLetter=$a;
$i=2;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'id baza');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'nazwa druzyny');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'region rankingu RTAM');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'Kapitan');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'Zawodnik 1');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'Zawodnik 2');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'email');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'telefon');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'ilość logowań');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'ost. Logowanie');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'data rejestracji');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'aktywny');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'nazwa dystrybutora');
$a++;

$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'ulica');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'dom');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'lokal');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'kod');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'miejscowość');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'NIP');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'zgoda 1');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'zgoda 2');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'zgoda 3');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'zgoda 4');
$a++;


$this->excel->getActiveSheet()->getStyle($this->_letters[$firstLetter].$i.':'.$this->_letters[$a].$i)->applyFromArray($this->_styleArray3); /* styl naglowkow*/

$users =$this->user_model->getList('*',array());


$i++;
	foreach($users as $item) {
		
		$a=1;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['id']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['login']);
		$a++;
		if ($item['idRank']) $this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_ph[$item['idRank']]);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['name1'].' '.$item['surname1']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['name2'].' '.$item['surname2']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['name3'].' '.$item['surname3']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['email']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['phone']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['loginCount']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['lastLogin']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['registerDate']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['active']]);
		$a++;

		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['firm']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['street']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['home']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['number']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['zip']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['city']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'`'.$item['nip']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['agree1']]);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['agree2']]);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['agree3']]);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['agree4']]);
		$a++;
		

		$i++;	
	}
	
$date = explode(' ' ,$this->_actDate);

$filename='uczestnicy_'.$date[0].'.xls'; 
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="'.$filename.'"'); 
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
$objWriter->save('php://output');	
}
function results() {
$this->excel->getActiveSheet()->setTitle('Arkusz 1');

$a=1;
$firstLetter=$a;
$i=1;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'id baza');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'nazwa druzyny');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'pojedynek wrzesień');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'misja październik');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'pojedynek październik');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'misja listopad');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'pojedynek listopad');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'misja grudzień');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'pojedynek grudzień');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'misja styczeń');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'pojedynek styczeń');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'misja luty');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'pojedynek luty');



$this->excel->getActiveSheet()->getStyle($this->_letters[$firstLetter].$i.':'.$this->_letters[$a].$i)->applyFromArray($this->_styleArray3); /* styl naglowkow*/

$users =$this->user_model->getList('id,login,mission1,mission2,mission3,mission4,mission5',array());
$pazdziernik= $this->user_model->getGameListSuate('winner',array('month'=>'pazdziernik','status'=>'zakonczony'));
$listopad= $this->user_model->getGameListSuate('winner',array('month'=>'listopad','status'=>'zakonczony'));
$grudzien= $this->user_model->getGameListSuate('winner',array('month'=>'grudzien','status'=>'zakonczony'));
$styczen= $this->user_model->getGameListSuate('winner',array('month'=>'styczen','status'=>'zakonczony'));
$luty= $this->user_model->getGameListSuate('winner',array('month'=>'luty','status'=>'zakonczony'));
$wrzesien= $this->user_model->getGameListSuate('winner',array('month'=>'wrzesien','status'=>'zakonczony'));

$pazdziernikArr = array();
foreach($pazdziernik as $item) {
	$pazdziernikArr[$item['winner']] = 1;
}
$listopadArr = array();
foreach($listopad as $item) {
	$listopadArr[$item['winner']] = 1;
}
$grudzienArr = array();
foreach($grudzien as $item) {
	$grudzienArr[$item['winner']] = 1;
}
$styczenArr = array();
foreach($styczen as $item) {
	$styczenArr[$item['winner']]= 1;
}
$wrzesienArr = array();
foreach($wrzesien as $item) {
	$wrzesienArr[$item['winner']]= 1;
}
$lutyArr = array();
foreach($luty as $item) {
	$lutyArr[$item['winner']]= 1;
}
$i++;
	foreach($users as $item) {
		
		$a=1;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['id']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['login']);
		$a++;
		if(!empty($wrzesienArr[$item['id']])) {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'tak');
		} else {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'nie');
		}
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['mission1']]);
		$a++;
		if(!empty($pazdziernikArr[$item['id']])) {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'tak');
		} else {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'nie');
		}
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['mission2']]);
		$a++;
		if(!empty($listopadArr[$item['id']])) {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'tak');
		} else {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'nie');
		}
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['mission3']]);
		$a++;
		if(!empty($grudzienArr[$item['id']])) {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'tak');
		} else {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'nie');
		}
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['mission4']]);
		$a++;
		if(!empty($styczenArr[$item['id']])) {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'tak');
		} else {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'nie');
		}
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_boolArr[$item['mission5']]);
		$a++;
		if(!empty($lutyArr[$item['id']])) {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'tak');
		} else {
			$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,'nie');
		}


		$i++;	
	}

$date = explode(' ' ,$this->_actDate);

$filename='uczestnicy_wyniki_'.$date[0].'.xls'; 
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="'.$filename.'"'); 
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
$objWriter->save('php://output');	
}
function ranking() {
$this->excel->getActiveSheet()->setTitle('Arkusz 1');

$a=1;
$firstLetter=$a;
$i=1;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'id baza');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'nazwa druzyny');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'nazwa dystrybutora');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'Liga');
$a++;
$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i, 'Wynik');
$a++;




$users =$this->user_model->getRanking('login,points,idUser,idRank,firm',array());

$i++;
	foreach($users as $item) {
		
		$a=1;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['idUser']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['login']);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['firm']);
		$a++;
		if ($item['idRank']) $this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$this->_ph[$item['idRank']]);
		$a++;
		$this->excel->getActiveSheet()->setCellValue($this->_letters[$a].$i,$item['points']);
		$a++;


		$i++;	
	}

$date = explode(' ' ,$this->_actDate);

$filename='ranking_'.$date[0].'.xls'; 
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="'.$filename.'"'); 
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
$objWriter->save('php://output');	
}
} /*koniec */?>
