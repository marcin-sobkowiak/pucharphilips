<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->_user['logged']) redirect('logowanie');
	}


function index()
{
	$rank = $this->user_model->getRanking('login,points,idUser',array('idRank'=>$this->_user['idRank']));
	$data=array('pageFile'=>'user/index.php','classBg'=>'bg-1','rank'=>$rank);
	$this->_display(1,$data);
}

function duels()
{
	$duels=array();
	/*wrzesien*/
	$pair = $this->user_model->getGameList('*',array('(idPlayer1='.$this->_user['idUser'].' OR idPlayer2='.$this->_user['idUser'].')'=>''));
	$win=0;
	$loss=0;
	foreach($pair as $item) {
		$game=array('month'=>$this->_monthArr[$item['month']],'player'=>$this->_user['login']);	
		if($item['idPlayer1']!=$this->_user['idUser']) {
			$opponent = $this->user_model->getSingle('login',array('id'=>$item['idPlayer1']));
			$points2 = $this->user_model->getSinglePoints('points',array('idUser'=>$item['idPlayer1'],'month'=>$item['month']));
			if(empty($points2)) $points2['points']=0;
		} else {
			$opponent = $this->user_model->getSingle('login',array('id'=>$item['idPlayer2']));
			$points2 = $this->user_model->getSinglePoints('points',array('idUser'=>$item['idPlayer2'],'month'=>$item['month']));
			if(empty($points2)) $points2['points']=0;
		}
		if($item['winner']!=0) {
			if($item['winner']==$this->_user['idUser']) { 
				$win++;
			} else {
				$loss++;
			}
		}
		$game['opponent'] = $opponent['login'];
		$game['status'] =$this->_statusArr[$item['status']];
		$points = $this->user_model->getSinglePoints('points',array('idUser'=>$this->_user['idUser'],'month'=>$item['month']));
		if(empty($points)) $points['points']=0;
		$game['points']= $points['points'];
		$game['points2']= $points2['points'];
		array_push($duels,$game);
	}
	$result=array('win'=>$win,'loss'=>$loss);
	$data=array('pageFile'=>'user/duels.php','classBg'=>'bg-1','duels'=>$duels,'result'=>$result);
	$this->_display(1,$data);
}
function mission()
{
	$data=array('pageFile'=>'user/mission.php','classBg'=>'bg-1');
	$this->_display(1,$data);
}
function changePass() /*przypomnienie hasła */ {
	   $config = array(
               array('field'   => 'nowe1','label'   => 'nowe hasło','rules'   => 'trim|required|xss_clean|min_length[8]'),
               array('field'   => 'nowe2','label'   => 'Powtórz hasło','rules'   => 'trim|required|xss_clean|matches[nowe1]')
            );
	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{
		$data=array('pageFile'=>'user/changePass.php','classBg'=>'bg-1');
		$this->_display(1,$data);
	}
	else
	{
		$this->db->update('users',array('password'=>$this->input->post('nowe1')),array('id'=>$this->_user['idUser']));
		$this->session->set_flashdata('popup','Nowe hasło zapisane');
		redirect('zmiana-hasla');
	}
}

}/* End of file  */