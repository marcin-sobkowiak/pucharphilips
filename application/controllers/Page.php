<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}


function index()
{
	$data=array('pageFile'=>'page/index.php','classBg'=>'');
	$this->_display(1,$data);
}

function prizes()
{
	$data=array('pageFile'=>'page/prizes.php','classBg'=>'bg-1');
	$this->_display(1,$data);
}

function contact()
{
	$config = array(
			array('field'   => 'content','label'   => 'content','rules'   => 'trim|required|xss_clean|min_length[3]'),
			array('field'   => 'email','label'   => 'E-mail','rules'   => 'trim|required|xss_clean|valid_email'),
			array('field'   => 'name','label'   => 'Imię i nazwisko','rules'   => 'trim|required|xss_clean'),
			array('field'   => 'trigger','label'   => 'trigger','rules'   => 'trim|required|xss_clean'),       
		);
	$this->form_validation->set_rules($config);	
	if ($this->form_validation->run() == FALSE)
	{	
		$data=array('pageFile'=>'page/contact.php','classBg'=>'bg-1');
		$this->_display(1,$data);
	} else {
		$time = time();
		$text = nl2br($this->input->post('content'));
		$agree1 =0;	
		//if($this->input->post('agree1')) $agree1=1;
		$agree1Text ='';	
		$agree2Text ='';
		$text = '<strong>Nadawca</strong><br/>
					Imie i nazwisko: '.$this->input->post('name').'<br/>
					E-mail: '.$this->input->post('email').'<br/>
					Wiadomość:<br/>'.$text;
		$insert = array(
				'session_id' => $this->session->session_id,
				'session_ip'=>$_SERVER['REMOTE_ADDR'],
				'session_browser'=>$this->input->user_agent(),
				'session_start'=>$time,
				'session_time'=>$time,
				'addDate'=>$this->_actDate,
				'agree1'=>$agree1,
				'agree1Text'=>$agree1Text,
				'agree2Text'=>$agree2Text,
				'content'=>$text,
				'name'=>$this->input->post('name'),
				'email'=>$this->input->post('email'),
		);
		$this->utilities4->_sendEmail($text,$this->_siteEmail,'Mail z formularza kontaktowego Puchar Philips',$this->_siteEmail,$fromTitle='Puchar Philips');
		$this->db->insert('contact',$insert);		
		$this->session->set_flashdata('popup','Dziękujemy za wysłanie wiadomości.');
		redirect('kontakt');
	}	
}
function duel()
{
	$data=array('pageFile'=>'page/duel.php','classBg'=>'');
	$this->_display(1,$data);
}
function mission()
{
	$data=array('pageFile'=>'page/mission.php','classBg'=>'');
	$this->_display(1,$data);
}

}/* End of file  */