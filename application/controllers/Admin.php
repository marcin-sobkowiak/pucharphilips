<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {
	function __construct() {
	parent::__construct();
		$this->load->model('admin_model');
	}

function index()
{
	if($this->_admin['logged']) redirect('admin_utilities');
	if (!$this->session->userdata('try')) {$this->session->set_userdata(array('try'=>0)); }/* ustawia ilość prób */
	$try=0;	/* ustawia ilość prób */
	$config = array(
               array('field'   => 'login','label'   => 'login','rules'   => 'trim|required|xss_clean'),
               array('field'   => 'password','label'   => 'password','rules'   => 'trim|required|xss_clean|max_length[30]')
            );
	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{
		$tablica = array('error'=>false);
		
		$this->_displayAdmin('admin/logowanie',$tablica);
	}
	else
	{
		$login = $this->input->post('login');
		$pass = sha1($this->input->post('password'));
		$logData = $this->admin_model->getSingle('*',array('login'=>$login,'pass'=>$pass,'active'=>1)); /* sprawdza czy poprawnosc loginu i hasla */
		if (!empty($logData) && $logData['active']) {  /* poprawne dane */
			$logCount = $logData['loginCount']+1; 
			$time = time();
			$insert = array(
					'session_id' => $this->session->session_id,
					'session_ip'=>$_SERVER['REMOTE_ADDR'],
					'session_browser'=>$this->input->user_agent(),
					'session_start'=>$time,
					'session_time'=>$time,
					'lastLogin'=>$this->_actDate,
					'loginCount' => $logCount  
				);
			$this->db->update('admins',$insert,array('idAdmin'=>$logData['idAdmin']));
			setcookie("admin_sid",$this->session->session_id,time()+72000,'/');
			$this->db->insert('adminsLogin',array('idAdmin'=>$logData['idAdmin'],'date'=>$this->_actDate));
			$this->session->set_userdata(array('lang'=>'pl')); 
			if($this->_actDate>$logData['passwordDate']) {
				redirect('admin_utilities');				
			} else {
				redirect('admin_utilities');	
			}
		}
		else /*bledne dane */
		{
			if($this->session->userdata('try')<3) {
				$loginCheck = $this->admin_model->getSingle('*',array('login'=>$login));/* sprawdza popranosc loginu */
				if (!empty($loginCheck))  {/*jezeli user istnieje */
					$this->session->set_userdata(array('try'=>$this->session->userdata('try')+1)); 
				}
				$tablica = array('error'=>1);
			} else {
			/* blokowanie konta */
				$tablica = array('error'=>2);
				$comment = 'zablokowane ze względu na dużą ilość błędnych prób '.$this->_actDate.' z ip: '.$_SERVER['REMOTE_ADDR'];
				$this->db->update('admins',array('active'=>0,'comment'=>$comment),array('login'=>$login));
			}
		$this->_displayAdmin('admin/logowanie',$tablica);
		}
	}
}

function logout()
{
	$this->load->library('session');
	$this->session->sess_destroy();
	setcookie('admin_sid', null, -1,'/');
	redirect('admin');
}	


}/*koniec controllera */

