<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cron extends MY_Controller {
	function __construct()
	{
		parent::__construct();
		//$this->load->library('simplexml');
		//$this->load->helper('xml');
		$this->output->enable_profiler(true);
	}
	

function index($lang='pl')
{
	$actRound =$this->utilities4->_actRound($lang,$this->_edition); /* aktualna runda */
	$actKnowledge = $this->challenge_model->getSingleKnowledge('prior,id',array('idRound'=>$actRound['idRound'],'lang'=>$lang,'dateFrom < '=>$this->_actDate,'dateTo > '=>$this->_actDate));
	
	if($actKnowledge['prior']==1) {  /* poprzednia runda */
		if($actRound['idRound']>1) {
			$round = $actRound['idRound']-1;
			$knowledgeToCount = $this->challenge_model->getSingleKnowledge('prior,id',array('idRound'=>$round,'lang'=>$lang,'prior'=>4));
		}
	} else { /* aktualna runda */
		$prior = $actKnowledge['prior'] -1;
		$round = $actRound['idRound'];
		$knowledgeToCount = $this->challenge_model->getSingleKnowledge('prior,id',array('idRound'=>$actRound['idRound'],'lang'=>$lang,'prior'=>$prior));
	
	}
	$users = $this->user_model->getList('id,level',array('lang'=>$lang,'active'=>1,'agree1'=>1)); /* uczestnicy */
	/* tworzenie jsona quizu */
	$poll=$this->challenge_model->getQuestions('id',array('active'=>1,'idKnowledge'=>$knowledgeToCount['id']));
	$result=array();
	foreach($poll as $item) {
		$question = array('id'=>$item['id'],'points'=>0,'answers'=>array(),'correct'=>false);
		$answers = $this->challenge_model->getAnswers('id,correct',array('idQuestion'=>$item['id'],'active'=>1));
		foreach($answers as $item2) {
			$item2['value'] = false;
			array_push($question['answers'],$item2);
		}
		array_push($result,$question);
	}

	foreach($users as $item) {
		$userResult = $this->challenge_model->getSingleResult('*',array('idUser'=>$item['id'],'idKnowledge'=>$knowledgeToCount['id'])); /* sprawdzenie wyników uczestnika z danego quizu */
		if(empty($userResult)) {
			$points = 0;
			if($item['level']=='starter') {
				$points = -1;
			} elseif($item['level']=='expert') {
				$points = -2;
			} elseif($item['level']=='champion') {
				$points = -3;
			}
			$data =array();
			/* tworzenie jsona  z odpowiedziami */
			foreach($result as $item3) {
				$pointsAnswer=0;
				$answers =array();
				foreach($item3['answers'] as $item2) {
					$insert = array('id'=>$item2['id'],'value'=>false,'correct'=>$item2['correct'],'points'=>0);
					array_push($answers,$insert);
				}
				$question = array('id'=>$item3['id'],'pointsAnswer'=>0,'answers'=>$answers);
				array_push($data,$question);
			}
			$data = json_encode($data);
			echo '<br/><br/>';
			echo $data;
			
			/* zapis do bazy */
			$insert =array(
						'idUser'=>$item['id'],
						'idKnowledge'=>$knowledgeToCount['id'],
						'idRound'=>$round,
						'points'=>$points,
						'edition'=>$this->_edition,
						);
			$insert2 = $insert;	
			$insert['type'] ='quiz';	
			$this->db->insert('rank',$insert);/* ranking */
			$insert2['result'] =$data;
			$insert2['solved'] =0;
			$insert2['level'] =$item['level'];
			$this->db->insert('quizResults',$insert2);/* wynik quizu */
			/* aktualizacja statusu */
			$points = $this->user_model->getSinglePoints('sum(points) as points',array('idUser'=>$item['id'],'edition'=>$this->_edition));/* pkt w rankingu */
			if($points['points']==NULL)$points['points']=0;
			$points=$points['points'];
			if($points<23)  {
				if($this->_user['level']!='starter') $this->db->update('users',array('level'=>'starter','levelActDate'=>$this->_actDate,'levelActRound'=>$round),array('id'=>$item['id']));
			} elseif($points>23&&$points<72) {
				if($this->_user['level']!='expert') $this->db->update('users',array('level'=>'expert','levelActDate'=>$this->_actDate,'levelActRound'=>$round),array('id'=>$item['id']));	
			} else {
				if($this->_user['level']!='champion') $this->db->update('users',array('level'=>'champion','levelActDate'=>$this->_actDate,'levelActRound'=>$round),array('id'=>$item['id']));	
			}
		}
	}
}
function turboKozak($lang='pl') {
	$actRound =$this->utilities4->_actRound($lang,$this->_edition); /* aktualna runda */
	if($actRound['idRound']>2) { /* w pierwszych 2 rundach nie ma mozliwosci bycia championem */
		$users = $this->user_model->getList('id,level,levelActRound',array('lang'=>$lang,'active'=>1,'agree1'=>1,'level'>'champion')); /* uczestnicy */
		foreach($users as $item) {
			$levelDiff = $item['levelActRound']- $actRound['idRound'];
			if($levelDiff >2 ) {
				if($actRound['idRound']<5) { 
					$turbo= $this->challenge_model->getSingleTurboAnswer('*',array('idTurbo'=>1,'idUser'=>$item['id'],'edition'=>$this->_edition));
					if(empty($turbo)) $this->db->insert('turboAnswer',array('idTurbo'=>1,'idUser'=>$item['id'],'edition'=>$this->_edition,'idRound'=>$actRound['idRound']));
				} else {
					$turbo= $this->challenge_model->getSingleTurboAnswer('*',array('idTurbo'=>1,'idUser'=>$item['id'],'edition'=>$this->_edition));
					if(empty($turbo)) {
						$this->db->insert('turboAnswer',array('idTurbo'=>1,'idUser'=>$item['id'],'edition'=>$this->_edition,'idRound'=>$actRound['idRound']));
					} else {	
						$turbo= $this->challenge_model->getSingleTurboAnswer('*',array('idTurbo'=>2,'idUser'=>$item['id'],'edition'=>$this->_edition));
						if(empty($turbo)) $this->db->insert('turboAnswer',array('idTurbo'=>2,'idUser'=>$item['id'],'edition'=>$this->_edition,'idRound'=>$actRound['idRound']));
					}
				}	
			}
		}
	}
}



}/* End of file  */