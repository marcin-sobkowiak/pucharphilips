<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MY_Controller {
	function __construct()
	{
		parent::__construct();
		
	}

function index($lang='')
{	
	if($this->_user['logged']) redirect('');
	$data=array('pageFile'=>'login/index.php','classBg'=>'bg-1');
	$config = array(
               array('field'   => 'password','label'   => 'Hasło','rules'   => 'trim|required|xss_clean|max_length[30]'),
               array('field'   => 'login','label'   => 'Adres e-mail','rules'   => 'trim|required|xss_clean'),
            );
	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{
		$data['error']=0;
	}
	else
	{
		$logResult=$this->auth4->logUser($this->input->post('login'),$this->input->post('password'),'user_sid',0);
		if ($logResult['logged']) {  /* poprawne dane */
			$result =  $this->user_model->getSingleGame('winner',array('month'=>'\'pazdziernik\'','(idPlayer1='.$logResult['idUser'].' OR idPlayer2='.$logResult['idUser'].')'=>''));
			if(!empty($result)) {
				if($result['winner']==$logResult['idUser']) { 
					//$text='Gratulujemy!<br/>Zwyciężyliście w II pojedynku sprzedażowym!<br/>Wasza karta prepaid zasilona na 50 zł niedługo do Was dotrze!';
				} else {
					//$text='Niestety, tym razem nie udało Wam się wygrać. Trzymamy kciuki za zwycięstwo w kolejnym pojedynku!';
				}
				$text = 'Rozliczyliśmy już grudniowe pojedynki sprzedażowe! Wejdź w zakładkę Pojedynki i sprawdź czy wygrałeś!';
				$this->session->set_flashdata('popup',$text);
			}
			redirect('');	 
		} else {/*bledne dane */
			$data['error']=1;
		}
	}
	$this->_display(1,$data);

}

function logout() /*wylogowanie */ {
	if($this->_user['logged']) {
		$insert = array(
					'session_id' =>  NULL,
					'session_ip'=>NULL,
					'session_browser'=> NULL,
				);
		$this->db->update('users',$insert,array('id'=>$this->_user['idUser']));		
	}			
	$this->session->sess_destroy();
	$this->cart->destroy();
	setcookie('user_sid', null, -1,'/');
	redirect('logowanie');	
}
function remind() /*przypomnienie hasła */ {
	if($this->_user['logged']) redirect('');
	$config = array(
               array('field'   => 'login','label'   => 'E-mail','rules'   => 'trim|required|xss_clean'),
               array('field'   => 'trigger','label'   => 'trigger','rules'   => 'trim|required|xss_clean'),
            );
	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{
		$data=array('pageFile'=>'login/remind.php','classBg'=>'bg-1');
		$this->_display(1,$data);
	}
	else
	{
		$user=$this->user_model->getSingle('id,login,email,password',array('email'=>$this->input->post('login'),'active'=>1));
		if (!empty($user)) {  /* poprawne dane */
			/*** email/sms ?? */
			$data=array('passwordText'=>$user['password'],'login'=>$user['email']);
			$text = $this->load->view('mail/remind',$data,true);
			$subject='Przypomnienie hasła';
			$this->utilities4->_sendEmail($text,$user['email'],$subject,$this->_siteEmail );
			$this->session->set_flashdata('popup','Na podany adres został wysłany e-mail z danymi do logowania');
		} else {
			$this->session->set_flashdata('popup','Nie ma takiego uczestnika');
		}
		redirect('logowanie');
	}
}
function register() /*rejestracja */ {
	if($this->_user['logged']) redirect('');
	$config = array(
		array('field'   => 'name1','label'   =>'Imię Kapitana','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'surname1','label'   =>'Nazwisko Kapitana','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'name2','label'   =>'Imię 1 zawodnika','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'surname2','label'   =>'Nazwisko 1 zawodnika','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'name2','label'   =>'Imię 2 zawodnika','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'surname2','label'   =>'Nazwisko 2 zawodnika','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'email','label'   => 'E-mail','rules'   => 'trim|required|xss_clean|valid_email|checkEmail'),
		array('field'   => 'phone','label'   => 'Telefon','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'firm','label'   => 'Nazwa dystrybutora','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'street','label'   => 'Ulica oddziału','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'home','label'   => 'Nr domu oddziału','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'number','label'   => 'Nr lokalu oddziału','rules'   => 'trim|xss_clean'),
		array('field'   => 'zip','label'   => 'Kod pocztowy oddziału','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'city','label'   => 'Miejscowość oddziału','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'login','label'   => 'Nazwa zespołu','rules'   => 'trim|required|xss_clean|checkLogin'),
		array('field'   => 'agree1','label'   =>'Oświadczenie zapoznaniu się z regulaminem','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'agree2','label'   =>'Zgoda właściciela hurtowni','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'agree3','label'   => 'Zgoda marketingowa','rules'   => 'trim|xss_clean'),
		array('field'   => 'agree4','label'   => 'Akceptacja zasad korzystania','rules'   => 'trim|required|xss_clean'),
		array('field'   => 'nip','label'   => 'NIP','rules'   => 'trim|required|xss_clean'),
            );

	$this->form_validation->set_rules($config);
	if ($this->form_validation->run() == FALSE)
	{
		$data=array('pageFile'=>'login/register.php','classBg'=>'bg-1');
		$this->_display(1,$data);
	} else {
		$agree1=0;
		$agree2=0;
		$agree3=0;
		$agree4=0;
		if($this->input->post('agree1')) $agree1=1;
		if($this->input->post('agree2')) $agree2=1;
		if($this->input->post('agree3')) $agree3=1;
		if($this->input->post('agree4')) $agree4=1;
		$insert = array(
					'name1'=>$this->input->post('name1'),
					'surname1'=>$this->input->post('surname1'),
					'name2'=>$this->input->post('name2'),
					'surname2'=>$this->input->post('surname2'),
					'name3'=>$this->input->post('name3'),
					'surname3'=>$this->input->post('surname3'),
					'email'=>$this->input->post('email'),
					'phone'=>$this->input->post('phone'),
					'login'=>$this->input->post('login'),
					'firm'=>$this->input->post('firm'),
					'street'=>$this->input->post('street'),
					'number'=>$this->input->post('number'),
					'home'=>$this->input->post('home'),
					'zip'=>$this->input->post('zip'),
					'city'=>$this->input->post('city'),
					'nip'=>$this->input->post('nip'),
					'agree1'=>$agree1,
					'agree2'=>$agree2,
					'agree3'=>$agree3,
					'agree4'=>$agree4,
					'active'=>1,
					'agree1Date'=>$this->_actDate,
					'agree2Date'=>$this->_actDate,
					'agree3Date'=>$this->_actDate,
					'agree4Date'=>$this->_actDate,
					);
		$pass = $this->auth4->passGenerate();
		$passwordText= $pass['passwordText'];
		$insert['password'] = $passwordText;
		$this->db->insert('users',$insert);
		/*** email/sms ?? */
		$data=array('passwordText'=>$passwordText,'login'=>$this->input->post('email'));
		$text = $this->load->view('mail/registered',$data,true);
		$subject='Rejestracja w programie';
		$this->utilities4->_sendEmail($text,$this->input->post('email'),$subject,$this->_siteEmail );
		$this->session->set_flashdata('popup','Dziękujemy za rejestrację. Na podany adres e-mail zostały wysłane dane do logowania');
		redirect('logowanie');				
	}
}

function error404()
{
	$data = array('pageFile'=>'login/error404.php','classBg'=>'bg-1');
	$this->_display(1,$data);

}

}/* End of file  */