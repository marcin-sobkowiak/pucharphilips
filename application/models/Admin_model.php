<?php

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
/* uzytkownicy */
  
function getList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('admins');
	$this->db->where($cond); 
	$this->db->order_by("idAdmin", "desc"); 
	$query = $this->db->get();
	return $query->result_array(); 			
}
function getSingle($data,$cond)	
{
	$this->db->select($data);
	$this->db->from('admins');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 			
}
function getSingleWorkerHours($data,$cond)	
{
	$this->db->select($data);
	$this->db->from('workerHours');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 			
}
function getAdminsInProjectList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('adminsInProject');
	$this->db->join('admins','adminsInProject.idAdmin=admins.idAdmin');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getProjectListByAdmin($data,$cond)
{
	$this->db->select($data);
	$this->db->from('adminsInProject');
	$this->db->join('projects','adminsInProject.idProject=projects.id');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getHours($data,$cond)
{
	$this->db->select($data);
	$this->db->from('adminsHoursInProject');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getAdminHoursInProject($data,$cond)
{
	$this->db->select($data);
	$this->db->from('adminsHoursInProject');
	$this->db->join('projects','adminsHoursInProject.idProject=projects.id');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
}/*koniec modelu */  
?>