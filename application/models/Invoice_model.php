<?php

class Invoice_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
function getSingle($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getListTrade($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->where($cond); 
	$this->db->group_by('tp_nip'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getListWithUsers($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->join('users','invoices.idUser=users.id');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleWithModels($data,$cond,$limit = 0)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->join('invoiceProducts','invoices.id=invoiceProducts.idInvoice');
	$this->db->where($cond); 
	if($limit) {
		$this->db->limit($limit);
	}
	$this->db->order_by('addDate','desc');
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getInvoiceProductsList($data,$cond,$limit = 0)
{
	$this->db->select($data);
	$this->db->from('invoiceProducts');
	$this->db->join('products','products.id=invoiceProducts.idProduct');
	$this->db->where($cond); 
	if($limit) {
		$this->db->limit($limit);
	}
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getInvoiceProductSingle($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoiceProducts');
	$this->db->join('products','products.id=invoiceProducts.idProduct');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getInvoiceProductsListGrouped($data,$cond,$limit = 0)
{
	$this->db->select($data);
	$this->db->from('invoiceProducts');
	$this->db->join('products','products.id=invoiceProducts.idProduct');
	$this->db->join('invoices','invoices.id=invoiceProducts.idInvoice');
	$this->db->where($cond); 
	if($limit) {
		$this->db->limit($limit);
	}
	$this->db->group_by('idProduct'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getRank($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->where($cond); 
	$this->db->group_by('idUser');	
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleProduct($data,$cond)
{
	$this->db->select($data);
	$this->db->from('products');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getProductList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('products');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleRealization($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->join('users', 'users.id = invoices.idUser');
	$this->db->join('invoiceProducts', 'invoiceProducts.idInvoice = invoices.id');		
	$this->db->where($cond); 
	$this->db->group_by('users.id'); 
	$this->db->order_by('sum(invoiceProducts.points*quantity)','desc'); 
	$this->db->order_by('max(date)','asc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleRealizationSum($data,$cond)
{
	$this->db->select($data);
	$this->db->from('invoices');
	$this->db->join('users', 'users.id = invoices.idUser');
	$this->db->join('invoiceProducts', 'invoiceProducts.idInvoice = invoices.id');		
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getFakturyView($data,$cond)
{
	$this->db->select($data);
	$this->db->from('faktury');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
}/*koniec modelu */  
?>