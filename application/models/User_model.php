<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
function getSingle($data,$cond)
{
	$this->db->select($data);
	$this->db->from('users');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('users');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleGame($data,$cond)
{
	$this->db->select($data);
	$this->db->from('gamePairing');
	$this->db->where($cond,null,false); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getGameList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('gamePairing');
	$this->db->where($cond,null,false); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getGameListSuate($data,$cond)
{
	$this->db->select($data);
	$this->db->from('gamePairing');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSinglePoints($data,$cond)
{
	$this->db->select($data);
	$this->db->from('results');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getRanking($data,$cond)
{
	$this->db->select($data);
	$this->db->from('ranking');
	$this->db->where($cond); 
	$this->db->order_by('points','desc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
}/*koniec modelu */  
?>