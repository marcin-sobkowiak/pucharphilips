<?php

class Page_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
  
function getSingle($data,$cond)
{
	$this->db->select($data);
	$this->db->from('pages');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('pages');
	$this->db->where($cond); 
	$this->db->order_by('prior','asc');
	$query = $this->db->get();
	return $query->result_array(); 				
}  
function getFirmList($data,$cond)
{
	$this->db->select($data);
	$this->db->from('firms');
	$this->db->where($cond); 
	$this->db->order_by('companyName','asc');
	$query = $this->db->get();
	return $query->result_array(); 				
} 
function getFirmSingle($data,$cond)
{
	$this->db->select($data);
	$this->db->from('firms');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
} 
}/*koniec modelu */  
?>