<?php

class Challenge_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
function getSingle($data,$cond)
{
	$this->db->select($data);
	$this->db->from('rounds');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getList($data,$cond,$order='idRound',$dir='asc')
{
	$this->db->select($data);
	$this->db->from('rounds');
	$this->db->where($cond); 
	$this->db->order_by($order,$dir); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleKnowledge($data,$cond)
{
	$this->db->select($data);
	$this->db->from('knowledge');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getListKnowledge($data,$cond,$order='prior',$dir='asc')
{
	$this->db->select($data);
	$this->db->from('knowledge');
	$this->db->where($cond); 
	$this->db->order_by($order,$dir); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleKnowledgeRead($data,$cond)
{
	$this->db->select($data);
	$this->db->from('knowledgeRead');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getSingleGallery($data,$cond)
{
	$this->db->select($data);
	$this->db->from('knowledgeGallery');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getListGallery($data,$cond,$order='prior',$dir='asc')
{
	$this->db->select($data);
	$this->db->from('knowledgeGallery');
	$this->db->where($cond); 
	$this->db->order_by($order,$dir); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
/* quiz */
function getQuestions($data,$cond)
{
	$this->db->select($data);
	$this->db->from('quizQuestions');	
	$this->db->where($cond); 
	$this->db->order_by('prior','asc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}

function getSingleQuestion($data,$cond)
{
	$this->db->select($data);
	$this->db->from('quizQuestions');	
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}

function getAnswers($data,$cond)
{
	$this->db->select($data);
	$this->db->from('quizAnswers');	
	$this->db->where($cond);
	$this->db->order_by('prior','asc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleAnswer($data,$cond)
{
	$this->db->select($data);
	$this->db->from('quizAnswers');	
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getSingleResult($data,$cond)
{
	$this->db->select($data);
	$this->db->from('quizResults');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getResults($data,$cond)
{
	$this->db->select($data);
	$this->db->from('quizResults');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getResultsRound($data,$cond)
{
	$this->db->select($data);
	$this->db->from('quizResults');
	$this->db->join('knowledge','knowledge.id=quizResults.idKnowledge');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
/* kola ratunkowe */
function getLifesaverQuestions($data,$cond)
{
	$this->db->select($data);
	$this->db->from('lifesaverQuestions');	
	$this->db->where($cond); 
	$this->db->order_by('prior','asc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleLifesaverQuestion($data,$cond)
{
	$this->db->select($data);
	$this->db->from('lifesaverQuestions');	
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getLifesaverAnswers($data,$cond)
{
	$this->db->select($data);
	$this->db->from('lifesaverAnswers');	
	$this->db->where($cond);
	$this->db->order_by('prior','asc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleLifesaverAnswer($data,$cond)
{
	$this->db->select($data);
	$this->db->from('lifesaverAnswers');	
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getSingleLifesaverResult($data,$cond)
{
	$this->db->select($data);
	$this->db->from('lifesaverResults');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getLifesaverResults($data,$cond)
{
	$this->db->select($data);
	$this->db->from('lifesaverResults');
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getTurboQuestion($data,$cond)
{
	$this->db->select($data);
	$this->db->from('turboQuestion');	
	$this->db->where($cond);
	$this->db->order_by('idTurbo','asc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleTurboQuestion($data,$cond)
{
	$this->db->select($data);
	$this->db->from('turboQuestion');	
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
function getTurboAnswer($data,$cond)
{
	$this->db->select($data);
	$this->db->from('turboAnswer');	
	$this->db->where($cond);
	$this->db->order_by('addDate','desc'); 
	$query = $this->db->get();
	return $query->result_array(); 				
}
function getSingleTurboAnswer($data,$cond)
{
	$this->db->select($data);
	$this->db->from('turboAnswer');	
	$this->db->where($cond); 
	$query = $this->db->get();
	return $query->row_array(); 				
}
}/*koniec modelu */  
?>