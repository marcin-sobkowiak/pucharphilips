<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$autoload['packages'] = array(APPPATH.'third_party');
$autoload['libraries'] = array('database','form_validation','session','cart','auth4','user4','utilities4');
$autoload['drivers'] = array();
$autoload['helper'] = array('url','form','text','security','string');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array('admin_model','page_model','user_model','challenge_model');
