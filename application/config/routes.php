<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'user';
$route['404_override'] = 'login/error404';
$route['translate_uri_dashes'] = FALSE;


$route['logowanie'] = 'login/index';
$route['rejestracja'] = 'login/register';
$route['przypominanie-hasla'] = 'login/remind';
$route['wyloguj'] = 'login/logout';

$route['zasady'] = 'page/index';
$route['nagrody'] = 'page/prizes';
$route['kontakt'] = 'page/contact';
$route['zasady/pojedynki'] = 'page/duel';
$route['zasady/misje-specjalne'] = 'page/mission';

$route['ranking'] = 'user/index';
$route['pojedynki'] = 'user/duels';
$route['misje-specjalne'] = 'user/mission';
$route['zmiana-hasla'] = 'user/changePass';